<?php
	include('dbconnect.php');
	if ( isset($_POST['last_name']) ) {
        $last_name = '%'.$_POST['last_name'].'%';
        if ($stmt = mysqli_prepare($connect, "SELECT s.std_id, s.last_name, s.first_name, g.name FROM students s LEFT JOIN std_group sg ON s.std_id = sg.std_id 
          LEFT JOIN groups g ON sg.group_id = g.group_id WHERE s.last_name LIKE ? AND g.is_custom = 0")) {
            mysqli_stmt_bind_param($stmt,"s",$last_name);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $id,$last_name,$first_name,$gName);
            $data = array();
            $i = 0;
            while ( mysqli_stmt_fetch($stmt) ){
                $data[$i] = array('id'=>$id,'last_name'=>$last_name, 'first_name'=>$first_name,'group_number'=>$gName);
                $i++;
            }
            echo json_encode($data);
            mysqli_stmt_close($stmt);
            mysqli_close($connect);
        }
	} else {
	    die();
    }
?>