<?php
	session_start();
	error_reporting (0);

	if ( isset($_GET['out']) ){
		session_unset('isAdmin');
		header("Location: ./");
	}
	if ( isset($_COOKIE['mpuStudent']) && !isset($_GET['id']) ){
		header("Location: ./?id={$_COOKIE['mpuStudent']}");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Новости</title>
	<link rel="stylesheet" href="css/main.css">
	<script src="js/jquery-1.12.3.min.js"></script>
</head>
<body>
	<div class="auth">
		<h2>Авторизация</h2>
		<form action="" method="POST">
			<input name="login" type="text" placeholder="Логин">
			<input name="password" type="password" placeholder="Пароль">
			<input type="submit" value="Войти">
		</form>
		<a href="">Закрыть</a>
	</div>	
	<div class="wrapper">
		<div class="header">		
			<?php
				if ( empty($_GET) ){ ?>
					<div class='toolbar'>
						<div class='contentWidth'>
							<div class='loginName'>
								<?php if ($_SESSION['user']){
									$last_name = $_SESSION['user']['last_name'];
									$first_name = substr($_SESSION['user']['first_name'], 0, 2);
									$patronymic = substr($_SESSION['user']['patronymic'], 0, 2);
									echo "<a href='admin'><span class='userName'>$last_name $first_name. $patronymic.</span></a>";
									if ($_SESSION['user']['is_admin']) echo "<span><a href='./admin'>Панель управления</span>";
									} ?>
							</div>
							<div class='log-in-out'>
									<?php if ($_SESSION['user']){
										echo "<span class='logOut'><a href='./?out'> Выйти </a></span>";
									} else {
										echo "<span class='logIn'><a href=''> Войти в систему </a></span>";
									} ?>
							</div>
						</div>				
					</div>
					<div class='title'>
							<div class='contentWidth'>
								<h1>Новости для студентов</h1>
							</div>				
						</div>
				<?php } ?>
		</div>
		<div class="content">
			<div class="contentWidth">
					<?php
						if ( isset($_GET['id']) ){
							include('dbconnect.php');
							$id = $_GET['id'];
							$newsCountPage = 4;
							if ( !isset($_GET['page']) ){
								$page = 0;
								$limit = '0,'.$newsCountPage;
							} else {
								$page = $_GET['page'];
								$limit = $newsCountPage * $page - $newsCountPage.','.$newsCountPage;
								
							}		
							// общее количество новостей для выбранного человека					
							$query1 = "SELECT * FROM news n LEFT JOIN teachers t ON n.author = t.tchr_id LEFT JOIN new_receiver c ON n.new_id = c.new_id LEFT JOIN students s ON c.std_id = s.std_id WHERE s.std_id = '$id'";
							$result = mysqli_query($connect, $query1);
							$newsCount = mysqli_num_rows($result);
							// сохраняем фамилию, имя студента, если еще не сохранены
							if ( $student_info = mysqli_fetch_assoc($result)){
								setcookie("mpuStudent", $student_info['std_id'], time()+60*60*24*2);
							}
							$pagesCount = (ceil ($newsCount / $newsCountPage) );
							// новости для выбранной страницы
							$query = "SELECT * FROM news n LEFT JOIN teachers t ON n.author = t.tchr_id LEFT JOIN new_receiver c ON n.new_id = c.new_id LEFT JOIN students s ON c.std_id = s.std_id WHERE s.std_id = $id ORDER BY n.new_id DESC LIMIT $limit";
							$result = mysqli_query($connect, $query) or die( mysqli_error($connect) );	
							$ids = [];

							// Создание XML
							$to_xml='<?xml version="1.0" encoding="utf-8"?>
										<news>';
							// $newsXML = new SimpleXMLElement();
							// $news = $newsXML->addChild('news');
							// $newsIntro = $newsXML->addChild('content');
							// $newsIntro->addAttribute('type', 'latest');
							// Header('Content-type: text/xml');
							
							//
					?>
						<div class="toBack">
							<a class="toBack__a" href=".">Вернуться к поиску</a>
						</div>
						<div class="news">
							<?php while ( $row = mysqli_fetch_array($result) ){
								$ids[] = $row['new_id'];
								$authorName = $row['tchr_last_name'].' '.substr($row['tchr_first_name'], 0, 2).'. '.substr($row['tchr_patronymic'], 0, 2).'.';
								
								// Добавляем XML
								$to_xml.= "<new>
								   <author>$authorName</author>
								   <pub_date>{$row['pub_date']}</pub_date>
								   <heading>{$row['heading']}</heading>
								   <text>{$row['text']}</text>
							   </new>";
								////////////////
							?>
								<div class='new <?php echo $row['read_status'] != 2 ? "news__new" : ""?>'>
									<div class='newContent'>
									<div class='upper'>
										<h3><?=$row['heading']?></h3>
										<div class='info'>
											<span class='newDate'><?=$row['pub_date']?></span>
											<span class='newAuthor'><?=$authorName?></span>
										</div>
									</div>

								<?php if ($row['img']){
									echo "<div class='newImg' style=\"background-image: url('{$row['img']}');\"></div>";
								}
									echo "<div class='newText'>{$row['text']}</div>
									<div class='clearfix'></div>";

									$query5 = "SELECT * FROM files WHERE new_id = {$row['new_id']}";
									$result2 = mysqli_query($connect, $query5);
									if ( mysqli_num_rows($result2) > 0 ){
										echo "<div class='files'> <ul>";
										
										while( $row2 = mysqli_fetch_assoc($result2) ){
											echo "<li><a href='{$row2['file_src']}'>{$row2['file_name']}</a></li>";
										}
										echo "</ul></div>";
									}									
									echo "
									<div class='actions'>";
									if ($row['read_status'] != 2){
										echo "<span data-std='{$row['std_id']}' data-new='{$row['new_id']}' class='read'>Понял</span>";
									}
									echo "</div>
									</div></div>";
							
							}

							$to_xml.= '</news>';
							// echo "$to_xml";


							echo "</div>";
							if ($pagesCount > 1){
								$currentPage = $_GET['page'];
								if (!$currentPage) $currentPage = 1;
								$nextPage = $currentPage+1;
								$prevPage = $currentPage-1;
								echo "<div class='paginator'>
									<ul>";
								if ( isset($_GET['page']) && $_GET['page'] != 1) echo "<li class='nextPage prevPage'><a href='./?id=$id&page=$prevPage'></a></li>";

								// если количество страниц менее 6, фомируем список с пропусками
								if ($pagesCount > 6){
									if ($currentPage > 2){
										echo "<li><a href='./?id=$id&page=1'>1</a></li>";
										if ($currentPage > 3){
											echo "<li>...</li>";
										}

									}
									if ($currentPage > 1 && $currentPage < 5){
										echo "<li><a href='./?id=$id&page=$prevPage'>$prevPage</a></li>";
									}
									if ($pagesCount - $currentPage > 3){
										echo "<li class='activePage'><a href='./?id=$id&page=$currentPage'>$currentPage</a></li>
													<li><a href='./?id=$id&page=$nextPage'>$nextPage</a></li><li>...</li>";
									} else {
										for ($i=$pagesCount-3; $i < $pagesCount; $i++){
											$next = $i+1;
											echo "<li";
											if ($currentPage == $i) echo " class='activePage'";
											echo "><a href='./?id=$id&page=$i'>$i</a></li>
														";
										}
									}
									echo "<li";
									if ($currentPage == $pagesCount) echo " class='activePage'";
									echo "><a href='./?id=$id&page=$pagesCount'>$pagesCount</a></li>";
								} else {
									$i = 1;
									while ( $i != $pagesCount+1){

										echo "<li";
										if ($i == $currentPage) echo " class='activePage'";
										echo "><a href='./?id=$id&page=$i'>$i</a></li>";
										$i++;
									}
								}

								if ($_GET['page'] != $pagesCount) echo "<li class='nextPage'><a href='./?id=$id&page=$nextPage'></a></li>";
								echo "</ul></div>";
							}
						} else { ?>
							<div class='searchBlock'>
								<div class='searchNews'>
									<input name='searchField' type='text' placeholder='Фамилия Имя' autocomplete='off'>
									<span class="searchNews__hint">Начните вводить фамилию</span>
									<div class='resultList'></div>
								</div></div></div>
						<?php }

						// $newsXML = new SimpleXMLElement("<news></news>");
						// $newsXML->addAttribute('newsPagePrefix', 'value goes here');
						// $newsIntro = $newsXML->addChild('content');
						// $newsIntro->addAttribute('type', 'latest');
						// Header('Content-type: text/xml');
						// echo $newsXML->asXML();

						// при открытии страницы меняем статус выводимых новостей на 1
						foreach ($ids as $key => $new_id) {
							// если у новости уже стоит статус 2, пропускаем ее
							$query1 = "SELECT * FROM new_receiver WHERE new_id = $new_id AND std_id = $id";
							$result = mysqli_query($connect, $query1) or die;
							$row = mysqli_fetch_assoc($result);
							if ($row['read_status'] == 2) continue;

							$query = "UPDATE new_receiver SET read_status = 1 WHERE new_id = $new_id AND std_id = $id";
							mysqli_query($connect, $query) or die;
						}
					?>			
				
			</div>
		</div>
		<div class="footer">
			
		</div>
	</div>
	<script src="js/script.min.js"></script>
	<script src="js/search.min.js"></script>
</body>
</html>