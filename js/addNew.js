var	wrapper = document.querySelector('.wrapper'),
	groupHeading = document.querySelector('.forGroup .heading'),
	filesHeading = document.querySelector('.leftSide .heading'),
	studentHeading = document.querySelector('.forStudent .heading'),
	newDate = document.querySelector('.leftSide input[name="date"]'),
	groups = document.querySelector('.forGroup .forContent'),
	students = document.querySelector('.forStudent .forContent'),
	selectAllGroups = document.querySelector('.forGroup .selectAll .all'),
	hideAllGroups = document.querySelector('.forGroup .selectAll .nothing'),
	selectAllStudents = document.querySelector('.forStudent .selectAll .all'),
	hideAllStudents = document.querySelector('.forStudent .selectAll .nothing'),
	rightSide = document.querySelector('.rightSide'),
	submitNew = document.querySelector('.submitBlock input'),
	heading = document.querySelector('input[name="heading"]'),
	textarea = document.querySelector('textarea[name="text"]');

heading.onblur = function(){
	if (this.value.length < 1){
		this.style.boxShadow = '0 0 2px #FF0000';
	} else {
		this.style.boxShadow = '0 0 1px #000000';
	}
};

textarea.onblur = function(){
	if (this.value.length < 1){
		this.style.boxShadow = '0 0 2px #FF0000';
	} else {
		this.style.boxShadow = '0 0 1px #000000';
	}
};

if (hideAllStudents){
	hideAllStudents.onclick = function(e){
		var students = document.querySelectorAll('input[name="StudentList[]"]'),
			i = 0,
			studentsLength = students.length,
			labels = document.querySelectorAll('.studentName'),
			g = 0,
			labelsLength = labels.length;
		for (; i < studentsLength; i += 1){
			students[i].checked = false;
		}
		for (; g < labelsLength; g += 1){
			labels[g].classList.remove('activeGroup');
		}
		e.preventDefault();
	}
}

if (hideAllGroups){
	hideAllGroups.onclick = function(e){
		for (gId in arStdsByGroupIds){
			for (sNum in arStdsByGroupIds[gId]){
				if (arStdsByGroupIds[gId][sNum]['hide']){
					arStdsByGroupIds[gId][sNum]['hide'] = false;
				}
			}
		}
		if (arRepeatedStds){
			for (sId in arRepeatedStds){
				arRepeatedStds[sId] = 0;
			}
		}
		document.querySelector('.forStudent .listItems').innerHTML = '';
		var labels = document.querySelectorAll('.groupName'),
			g = 0,
			labelsLength = labels.length;
		for (; g < labelsLength; g += 1){
			labels[g].classList.remove('activeGroup');
		}
		e.preventDefault();
	}
}

if (selectAllStudents){
	selectAllStudents.onclick = function(e){
		var students = document.querySelectorAll('input[name="StudentList[]"]'),
			labels = document.querySelectorAll('.studentName'),
			labelsLength = labels.length,
			i = 0,
			g = 0,
			studentsLength = students.length;
		for (; i < studentsLength; i += 1){
			students[i].checked = true;
		}
		for (; g < labelsLength; g += 1){
			labels[g].classList.add('activeGroup');
		}
		e.preventDefault();
	}
}
/* Формируем список студентов. Ключ - id группы, значение - список студентов */
var arStdsByGroupIds = [],
	arRepeatedStds = [];

(function studentsInit(){
	var groups = document.querySelectorAll('input[name="groupList[]"]'),
		i = 0,
		groupLength = groups.length,
		arGroupIds = [];
	for (; i < groupLength; i+=1){
		arGroupIds.push(groups[i].dataset.id);
	}
	$.ajax({
		url: 'takeStd.php',
		type: 'POST',
		data: {arGroupIds:arGroupIds},
		success: function(data){
			data = jQuery.parseJSON(data);
			arStdsByGroupIds = data.stdList;
			arRepeatedStds = data.repeatedStds;
		}
	});
})();
/* */
if (selectAllGroups){
	selectAllGroups.onclick = function(e){
		if (arRepeatedStds){
			for (sId in arRepeatedStds){
				arRepeatedStds[sId] = 0;
			}
		}
		var studentListItems = document.querySelector('.forStudent .listItems');
		studentListItems.innerHTML = '';
		var groups = document.querySelectorAll('input[name="groupList[]"]'),
			labels = document.querySelectorAll('.groupName'),
		    i = 0,
			g = 0,
			labelsLength = labels.length,
			groupLength = groups.length;
		for (; i < groupLength; i+=1){
			groups[i].checked = true;
		}
		for (; g < labelsLength; g+=1){
			labels[g].classList.add('activeGroup');
		}
		var groupId,
			k,
			fragment = document.createDocumentFragment(); // обертка для списка студентов, чтобы в цикле не обращаться к DOM
		for (groupId in arStdsByGroupIds){
			groupLength = arStdsByGroupIds[groupId].length;
			for (k = 0; k < groupLength; k += 1){
				label = document.createElement('label');
				label.dataset.group = arStdsByGroupIds[groupId][k]['group'];
				label.innerHTML = arStdsByGroupIds[groupId][k]['name'];
				label.className = 'studentName activeGroup';

				input = document.createElement('input');
				input.type = 'checkbox';
				input.value = arStdsByGroupIds[groupId][k]['name'];
				input.dataset.id = arStdsByGroupIds[groupId][k]['sId'];
				input.name = 'StudentList[]';
				input.hidden = true;
				input.checked = true;
				label.appendChild(input);
				if ( arStdsByGroupIds[groupId][k]['sId'] in arRepeatedStds ){
					arRepeatedStds[arStdsByGroupIds[groupId][k]['sId']] += 1;
					if (arRepeatedStds[arStdsByGroupIds[groupId][k]['sId']] >= 2){
						arStdsByGroupIds[groupId][k]['hide'] = true;
						label.style.display = 'none';
					}
				}
				fragment.appendChild(label); // добавляем студента в нашу обертку
			}
		}
		studentListItems.appendChild(fragment);
		submitNew.classList.remove('inactive');
		arCurStds = arStdsByGroupIds;
		e.preventDefault();
	}
}

if (newDate){
	var date = new Date(),
	    day = date.getDate(),
	    month = date.getMonth(),
	    year = date.getFullYear();
	day = '' + day;
	months = {0: 'января',1: 'февраля',2: 'марта',3: 'апреля',4: 'мая',5: 'июня',6: 'июля',7: 'августа',8: 'сентября',9: 'октября',
	10: 'ноября',11: 'декабря'};
	month = months[month];
	newDate.value = day + ' ' + month + ' ' + year;
}

if (filesHeading){
	filesHeading.onclick = function(){
		var classArr = this.classList;

		if ( classArr.contains('openHeading') ){
			classArr.remove('openHeading');
			this.nextElementSibling.style.display = 'none';
		} else{
			classArr.add('openHeading');
			this.nextElementSibling.style.display = 'block';
		}
	}
}

if (groupHeading){
	groupHeading.onclick = function(){
		var classArr = this.classList;
		if ( classArr.contains('openHeading') ){
			classArr.remove('openHeading');
			this.nextElementSibling.style.display = 'none';
		} else{
			classArr.add('openHeading');
			this.nextElementSibling.style.display = 'block';
		}
	}
}

if (studentHeading){
	studentHeading.onclick = function(){
		var classArr = this.classList;
		if ( classArr.contains('openHeading') ){
			classArr.remove('openHeading');
			this.nextElementSibling.style.display = 'none';
		} else{
			classArr.add('openHeading');
			this.nextElementSibling.style.display = 'block';
		}
	}
}

students.onmousedown = function(e){
	target = e.target;
	if ( !target.classList.contains('studentName') ) return;

	target.onselectstart = function(){
		return false;
	};
	if ( !target.classList.contains('activeGroup') ){
		if (target.firstElementChild.dataset.id in arRepeatedStds){
			hidenInputs = document.querySelectorAll('.studentName input[data-id="'+target.firstElementChild.dataset.id+'"]');
			[].forEach.call(hidenInputs, function(item){
				item.checked = true;
			})
		}
		target.firstElementChild.checked = false;
	} else {
		if (target.firstElementChild.dataset.id in arRepeatedStds){
			hidenInputs = document.querySelectorAll('.studentName input[data-id="'+target.firstElementChild.dataset.id+'"]');
			[].forEach.call(hidenInputs, function(item){
				item.checked = false;
			})
		}
		target.firstElementChild.checked = true;
	}
	target.classList.toggle('activeGroup');
};

function fillStdList(arSelectedStds){
	var studentListItems = document.querySelector('.forStudent .listItems'),
		fragment = document.createDocumentFragment(); // обертка для списка студентов, чтобы в цикле не обращаться к DOM
	arSelectedStds.forEach(function(item){
		label = document.createElement('label');
		label.dataset.group = item['group'];
		label.innerHTML = item['name'];
		label.className = 'studentName activeGroup';

		input = document.createElement('input');
		input.type = 'checkbox';
		input.value = item['name'];
		input.dataset.id = item['sId'];
		input.name = 'StudentList[]';
		input.hidden = true;
		input.checked = true;
		label.appendChild(input);
		if (item.hide){
			label.style.display = 'none';
		}
		fragment.appendChild(label); // добавляем студента в нашу обертку
	});
	studentListItems.appendChild(fragment);
}

var arCurStds = [];
if (groups){
	groups.onmousedown = function(e){
		target = e.target;
		if ( !target.classList.contains('groupName') ) return;
		target.onselectstart = function(){
			return false;
		};
		var groupId = target.firstElementChild.dataset.id;
		if (arStdsByGroupIds[groupId]){
			if ( !target.classList.contains('activeGroup') ){
				arStdsByGroupIds[groupId].forEach(function(item){
					if ( item.sId in arRepeatedStds ){
						arRepeatedStds[item.sId] += 1;
						if (arRepeatedStds[item.sId] >= 2){
							item.hide = true;
						}
					}
				});
				arCurStds[groupId] = arStdsByGroupIds[groupId];
				fillStdList(arCurStds[groupId]);
			} else {
				var groupName = target.firstElementChild.value;
				arCurStds[groupId].forEach(function (item) {
					document.querySelector('label[data-group="' + groupName + '"] input[data-id="' + item.sId + '"]').parentNode.remove();
					if (item.sId in arRepeatedStds) {
						arRepeatedStds[item.sId] -= 1;
						if (arRepeatedStds[item.sId] >= 1) {
							document.querySelector('.forStudent .listItems input[data-id="' + item.sId + '"]').parentNode.style.display = 'inline-block';
						}
						item.hide = false;
					}
				});
			}
		}
		target.classList.toggle('activeGroup');
	}
}

rightSide.onclick = function(){
	var students = document.querySelectorAll('input[name="StudentList[]"]');
	var inactive = 0;
	for (var i = 0; i < students.length; i++){
		if (students[i].checked == true){
			submitNew.classList.remove('inactive');
			return;
		} else{
			inactive++;
		}
	}
	if (inactive == students.length){
		submitNew.classList.add('inactive');
	}
};

submitNew.onclick = function(){
	if (this.classList.contains('inactive')){
		alert('Укажите адресата');
		return false;
	} 
	if (heading.value.length < 1){
		heading.style.boxShadow = '0 0 2px #FF0000';
		alert('Не указан заголовок');
		return false;
	}
	if (textarea.value.length < 1){
		textarea.style.boxShadow = '0 0 2px #FF0000';
		alert('Не указан текст');
		return false;
	}
	var form = document.querySelector('form'),
		formData = new FormData(form),
		students = document.querySelectorAll('input[name="StudentList[]"]:checked'),
		stdList = [],
		studentsLength = students.length,
		i = 0,
		xhr = new XMLHttpRequest();
	for (; i < studentsLength; i+=1){
		stdList.push(students[i].dataset.id);
	}
	stdList = JSON.stringify(stdList);
	formData.append("students", stdList);
	xhr.open("POST", "addNew.php");
	xhr.onreadystatechange = function(){
		document.querySelector('.successImg').style.transform = 'scale(1)';
		data = xhr.responseText;
		setTimeout(function(){
			location.reload();
		}, 1200);
	};
	xhr.send(formData);
	return false;
};