// скрываем/показываем крестик при наведении на строку
trs = document.querySelectorAll('.admin-list tr:not(:first-child)');
[].forEach.call(trs, function(item){
	item.addEventListener("mouseover", function showDelete(){
		this.lastElementChild.firstElementChild.style.display = 'inline';
		this.lastElementChild.lastElementChild.style.display = 'inline';
	});
	item.addEventListener("mouseout", function hideDelete(){
		this.lastElementChild.firstElementChild.style.display = 'none';
		this.lastElementChild.lastElementChild.style.display = 'none';
	});
});
function checkFieldLength(field,length,errorMsg){
	if (field.value.trim().split(' ').length != length){
		alert(errorMsg);
		field.focus();
		return false;
	}
	return true;
}
// добавление нового админа
if ( document.querySelector('.new-admin form') ){
	document.querySelector('.new-admin form').onsubmit = function(){
		var login = document.querySelector('.new-admin__login'),
			levelInputs = document.querySelectorAll('.new-admin input[name="level[]"]:checked'),
			level = 1,
			password = document.querySelector('.new-admin__password'),
			email = document.querySelector('.new-admin__email'),
			errorMsg = 'Логин должен состоять из ФИО';
		if ( !checkFieldLength(login,3,errorMsg) ){
			return false;
		}
		for (var i = 0; i < levelInputs.length; i++){
			if (levelInputs[i].value == 2)
				level = 2;
			else if (levelInputs[i].value == 3)
				level = 3;
		}
		var name = login.value.split(' '),
			lastName = name[0],
			firstName = name[1],
			patronymic = name[2],
		 	newAdmin = {
				lastName: lastName,
				firstName: firstName,
				patronymic: patronymic,
				password: password.value,
				email: email.value,
				level: level
			};
		$.ajax({
			url: 'admins.php',
			type: 'POST',
			data: newAdmin,
			success: function(){
				var success = document.createElement('span');
				success.innerHTML = 'Администратор добавлен!';
				document.querySelector('.new-admin fieldset').appendChild(success);
				setTimeout( function(){
					location.reload();
				}, 1000 )
			}
		});
		return false;
	}
}
if ( document.querySelector('.new-admin__login') ){
	document.querySelector('.new-admin__login').onblur = function(){
		if (this.value.trim().split(' ').length != 3){
			this.style.borderColor = 'rgba(255,0,0,0.4)';
			this.title = 'Логин должен состоять из ФИО';
		} else {
			this.style.borderColor = '';
		}
	};
}
// обработчик на удаление администратора
var deleteIcons = document.querySelectorAll('td .admin-delete');
[].forEach.call(deleteIcons, function(item){
	item.addEventListener("click", function (){
		if ( !confirm("Подтвердите удаление администратора") ) return false;
		var adminId = item.dataset.id;
		$.ajax({
		url: 'admins.php',
		type: 'POST',
		data: {adminId : adminId},
		success: function(){
				location.reload();
			}
		})
});
});

var editIcons = document.querySelectorAll('td .admin-edit');
[].forEach.call(editIcons, function(item){
	item.addEventListener("click", function (e){
		var target = e.target,
			tagName = target.tagName,
			adminId = item.dataset.id;

		while (tagName != 'TR'){
			target = target.parentNode;
			tagName = target.tagName;
		}
		var nextTr = target.nextElementSibling,
			curTr = target;
		target.remove();

		var login = target.querySelector('.admin-login').innerHTML,
			level = target.querySelectorAll('.admin-level img'),
			email = target.querySelector('.admin-email').innerHTML,
			avaSrc = target.querySelector('.admin-ava img').src,
			fragment = document.createDocumentFragment(),
			newTr1,
			privileges = {
				2: false,
				3: false
			};
		[].forEach.call(level, function(img){ // сохраняем текущие уровни доступа
			privileges[img.dataset.level] = 'checked';
		});
		newTr1 = document.createElement('tr');
		newTr1.className = 'editableTr';
		newTr1.innerHTML = '<td><img src="' + avaSrc + '" alt=""></td>\
							<td><input type="text" name="login" value="'+login+'"></td>\
							<td><input type="text" name="email" value="'+email+'"></td>\
							<td class="admin-level">\
								<label>\
									<input type="checkbox" name="level[]" value="2"'+privileges[2]+'>\
									<img src="/project/img/mng_gr2.png" alt="Управление группами" title="Управление группами">\
								</label>\
								<label>\
									<input type="checkbox" name="level[]" value="3"'+privileges[3]+'>\
									<img src="/project/img/cr_user.png" alt="Управление администраторами" title="Управление администраторами">\
								</label>\
							</td>\
							<td><img class="admin-delete" src="../img/close.png" title="Удалить"></td>';
		newTr2 = document.createElement('tr');
		newTr2.className = 'editableTr';
		newTr2.innerHTML = '<td></td>\
							<td colspan="2"><input type="password" name="password" placeholder="Новый пароль"><input type="password" name="confPassword" placeholder="Подтвердите пароль"></td>\
							<td class="admin-level"><a class="saveEdit" href="">OK</a><a class="cancelEdit" href="">Отмена</a></td>\
							<td></td>';
		document.querySelector('.admin-list tbody').insertBefore(newTr1, nextTr);
		document.querySelector('.admin-list tbody').insertBefore(newTr2, nextTr);
		newTr1.querySelector('.admin-level').appendChild(fragment);

		document.querySelector('.cancelEdit').onclick = function(){
			newTr1.remove();
			newTr2.remove();
			document.querySelector('.admin-list tbody').insertBefore(curTr, nextTr);
			return false;
		};

		document.querySelector('.saveEdit').onclick = function(){
			var login = document.querySelector('.editableTr input[name="login"]').value,
				email = document.querySelector('.editableTr input[name="email"]').value,
				levelInputs = document.querySelectorAll('.editableTr input[name="level[]"]:checked'),
				level = 1,
				password = document.querySelector('.editableTr input[name="password"]').value;

			for (var i = 0; i < levelInputs.length; i++){
				if (levelInputs[i].value == 2)
					level = 2;
				else if (levelInputs[i].value == 3)
					level = 3;
			}
			if (password){
				var confPassword = document.querySelector('.editableTr input[name="confPassword"]').value;
				if (password != confPassword){
					alert('Пароли не совпадают!');
					return false;
				}
			}
			var adminInfo = {
				id: adminId,
				login: login,
				email: email,
				level: level,
				password: password
			};
			$.ajax({
				url: 'admins.php',
				type: 'POST',
				data: {adminEdit: adminInfo},
				success: function(){
					newTr1.remove();
					newTr2.remove();
					curTr.querySelector('.admin-login').innerHTML = login;
					if (level > 1){
						curTr.querySelector('.admin-level').innerHTML = '<img src="/project/img/mng_gr2.png" alt="Управление группами" title="Управление группами" data-level="2">';
						if (level > 2){
							curTr.querySelector('.admin-level').innerHTML += '<img src="/project/img/cr_user.png" alt="Управление администраторами" title="Управление администраторами" data-level="3">';
						}
					} else {
						curTr.querySelector('.admin-level').innerHTML = '';
					}
					curTr.querySelector('.admin-email').innerHTML = email;
					document.querySelector('.admin-list tbody').insertBefore(curTr, nextTr);
				}
			});
			return false;
		}
});
	
});
if ( document.querySelector('.admins_control') ){
	document.querySelector('.admins_control').onchange = function(){
		if (this.checked)
			document.querySelector('.groups_control').checked = true;
	}
}

if ( document.querySelector('.groups_control') ){
	document.querySelector('.groups_control').onchange = function(){
		if (!this.checked)
			document.querySelector('.admins_control').checked = false;
	}
}

// Обработчик на добавление кнопки при изменении полей
var profileInputs = document.querySelectorAll('.prof-data input');
var profEditBut = document.querySelector('.prof-edit-but');
[].forEach.call( profileInputs, function(item){
	item.addEventListener('keydown', function(){
		profEditBut.style.display = 'inline-block';
	})
} );
document.getElementById('ava').onchange = function(){
	document.querySelector('label[for="ava"] span').innerHTML = this.value.split( '\\' ).pop();
	profEditBut.style.display = 'inline-block';
};
if ( document.querySelector('.profile__form') ){
	document.querySelector('.profile__form').onsubmit = function(){
		var login = document.querySelector('.prof-data__login'),
			errorMsg = 'Логин должен состоять из ФИО!';
		if ( !checkFieldLength(login,3,errorMsg) ){
			return false;
		}
	};
}
