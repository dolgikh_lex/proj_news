document.querySelector('.students tr:first-child').onclick = function(e){
	target = e.target;

	// type = target.dataset.sort;
	order = target.dataset.order;
	table = document.querySelector('.students');
	tbody = document.querySelector('.students tbody');	
	// сортировка таблицы
	sortTable(target.cellIndex, order);
	

	function sortTable(colNum, order) {
      var newOrder;
	  var tbody = document.querySelector('.students tbody');

	  // Составить массив из TR
	  var rowsArray = [].slice.call(tbody.rows);
	  // Отрежем первую строку (шапку)
	  rowsArray = rowsArray.slice(1);
	  var compare;	 

	  if (order == 'ask'){
	  	newOrder = 'desc';
	  	compare = function(rowA, rowB) {
	  	  return rowA.cells[colNum].innerHTML > rowB.cells[colNum].innerHTML ? 1 : -1;
	  	};
	  } else {
	  	newOrder = 'ask';
	  	compare = function(rowA, rowB) {
	  	  return rowA.cells[colNum].innerHTML < rowB.cells[colNum].innerHTML ? 1 : -1;
	  	};
	  }
	  
	  rowsArray.sort(compare);

	  table.removeChild(tbody);

	  for (var i = 0; i < rowsArray.length; i++) {
	    tbody.appendChild(rowsArray[i]);
	  }

	  table.appendChild(tbody);
	  target.dataset.order = newOrder;
	}
}