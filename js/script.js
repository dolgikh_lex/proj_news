var logIn = document.querySelector('.logIn a'),
	wrapper = document.querySelector('.wrapper'),
	auth = document.querySelector('.auth'),
	authClose = document.querySelector('.auth a'),
	inputLogin = document.querySelector('.auth input[type="text"]'),
	inputPassword = document.querySelector('.auth input[type="password"]'),
	submitLogin = document.querySelector('.auth input[type="submit"]'),
	toBackLink = document.querySelector('.toBack__a');

document.onclick = function(e){
	var target = e.target;
	if (target.parentNode.className == 'logIn' && target.tagName == 'A'){
		auth.style.top = 20 + '%';
		auth.style.opacity = 1;
		wrapper.style.opacity = 0.5;
		return false;
	} else if (target.parentNode.tagName == 'FORM' || target.className == 'auth'){
		return;
	} else if (target.parentNode.className != 'auth'){
		auth.style.top = -50 + '%';
		wrapper.style.opacity = 1;
	}
};
if (authClose){
	authClose.onclick = function(){
		auth.style.top = -50 + '%';
		wrapper.style.opacity = 1;
		return false;
	}
}

if (submitLogin){
	submitLogin.onclick = function(){
		var password = inputPassword.value;
		var arLogin = inputLogin.value.split(' ');
		if (arLogin.length != 3){
			alert('Введите ФИО!');
			return false;
		}
		var last_name = arLogin[0];
		var first_name = arLogin[1];
		var patronymic = arLogin[2];
		var send = {
			last_name:last_name,
			first_name:first_name,
			patronymic:patronymic,
			password:password
		};

		$.ajax({
			url: 'logInOut.php',
			type: 'POST',
			data: send,
			success: function(data){
				if (data){
					alert(data);
				} else {
					location.href="./admin";
				}
			}
		});
		return false;
	}
}

var news = document.querySelectorAll('.newContent');
for (var i = 0; i < news.length; i++){
	if (news[i].scrollHeight > news[i].clientHeight){
		fullText = document.createElement('a');
		fullText.innerHTML = 'Раскрыть полностью';
		fullText.className = 'readText';
		news[i].parentNode.appendChild(fullText);
	} 
}

wrapper.onclick = function(e){
	var target = e.target;
	if ( target.classList.contains('readText') ){
		target.previousElementSibling.style.maxHeight = target.previousElementSibling.scrollHeight + 'px';
		target.style.display = 'none';
	} else if ( target.classList.contains('read') ) {
		var stdId = target.dataset.std;
		var newId = target.dataset.new;
		var send = {
			std: stdId,
			new: newId
		};
		$.ajax({
			url: 'newStatus.php',
			type: 'POST',
			data: send,
			success: function(data){
				target.parentNode.removeChild(target);
			}
		})
	} else return;
};

if (toBackLink){
	toBackLink.onclick = function(){
		function setCookie(name, value, options) {
			options = options || {};

			var expires = options.expires;

			if (typeof expires == "number" && expires) {
				var d = new Date();
				d.setTime(d.getTime() + expires * 1000);
				expires = options.expires = d;
			}
			if (expires && expires.toUTCString) {
				options.expires = expires.toUTCString();
			}

			value = encodeURIComponent(value);

			var updatedCookie = name + "=" + value;

			for (var propName in options) {
				updatedCookie += "; " + propName;
				var propValue = options[propName];
				if (propValue !== true) {
					updatedCookie += "=" + propValue;
				}
			}

			document.cookie = updatedCookie;
		}
		function deleteCookie(name) {
			setCookie(name, "", {
				expires: -1
			})
		}
		deleteCookie('mpuStudent');
	};

}