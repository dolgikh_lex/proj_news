var arStdsByGroupIds = [], saveStds,
	groups = document.querySelectorAll('.groups .group');
if (groups.length > 1){
	(function studentsInit(){
		var groups = document.querySelectorAll('.groups .group'),
			i = 0,
			groupLength = groups.length,
			arGroupIds = [];
		for (; i < groupLength; i+=1){
			if (!groups[i].dataset.id){
				continue;
			}
			arGroupIds.push(groups[i].dataset.id);
		}
		$.ajax({
			url: '/project/admin/takeStd.php',
			type: 'POST',
			data: {arGroupIds:arGroupIds, activity: true},
			success: function(data){
				data = jQuery.parseJSON(data);
				arStdsByGroupIds = data.stdList;
			}
		});
	})();
}

var groupNameField = document.querySelector('.editGroupName');
var saveGroupName = document.querySelector('.saveGroupName');
var rightBlock = document.querySelector('.rightBlock');
var deleteGroup = document.querySelector('.deleteGroup');

// скрываем/показываем крестик при наведении на строку
trs = document.querySelectorAll('.stdList tr:not(:first-child)');
[].forEach.call(trs, function(item){
	item.addEventListener("mouseover", function showDelete(){
		this.lastElementChild.firstElementChild.style.display = 'inline';
	});
	item.addEventListener("mouseout", function hideDelete(){
		this.lastElementChild.firstElementChild.style.display = 'none';
	});
});

// события на показ кнопки "сохранить" при изменении имени студента
var namess = document.querySelectorAll('.stdList tr:not(:first-child) td:first-child');
[].forEach.call( namess, function(item,i,arr){
	item.addEventListener("keydown", function (){
		document.querySelector('.saveStds').style.display = 'inline-block';
		this.dataset.changed = 'true';
	});
} );

if (deleteGroup) {
	deleteGroup.onclick = function(){
		if ( !confirm("Удалить группу '"+groupNameField.value + "'?") ) return false;
		var groupId = document.querySelector('.activeGroup').dataset.id;
		$.ajax({
			url: 'delete.php',
			type: 'POST',
			data: { groupId: groupId },
			success: function(){
				location.reload();
			}
		})
	}
}

var html = document.querySelector('.groupInfo').innerHTML;
document.querySelector('.leftBlock').onclick = function(e){
	arNewCustomStds = [];
	stdList = [];
	target = e.target;
	value = target.value;

	if ( !target.classList.contains('group') || target.classList.contains('newGroup') ) return;
	if (!target.classList.contains('newGroup')){
		// document.querySelector('.rightBlock').innerHTML = '<table class="stdList"></table><div class="editStds"><a class="addStd" href="">Добавить студента</a><a class="saveStds" href="">Сохранить</a></div>';
		// document.querySelector('.addStd').onclick = addStdToTable;
	}
	if (!document.querySelector('.stdList')){
		document.querySelector('.rightBlock').innerHTML = '<table class="stdList"></table><div class="editStds"><a class="addStd" href="">Добавить студента</a><a class="saveStds" href="">Сохранить</a></div>';
		document.querySelector('.addStd').onclick = addStdToTable;
		document.querySelector('.stdList').onclick = function(e){
			deleteStd(e);
		}
	}
	var saveStdsBtn = document.querySelector('.saveStds'),
		newStdBtn = document.querySelector('.addStd');
	if (saveStdsBtn){
		saveStdsBtn.onclick = saveStds;
		saveStdsBtn.style.display = 'none';
	}
	if (newStdBtn){
		newStdBtn.style.display = '';
	}

	if ( target.classList.contains('newGroup') ){
		[].forEach.call( groups, function(item){
			if ( item.classList.contains('activeGroup') ){
				item.classList.remove('activeGroup');
				return;
			}
		});
		return;
	}
	groupNameField.value = value;
	groupNameField.focus();

	if (document.querySelector('.newGroup').style.display == 'none'){
		document.querySelector('.newGroup').style.display = 'inline-block';
		document.querySelector('.groupInfo').innerHTML = html;
		groupNameField = document.querySelector('.editGroupName');
		document.querySelector('.newGroupLabel').remove();
		document.querySelector('.hintGroupType').remove();
		document.querySelector('.editGroup .action').style.display = 'block';
	}

	[].forEach.call( groups, function(item){
		if ( item.classList.contains('activeGroup') ){
			item.classList.remove('activeGroup');
			return;
		}
	});
	target.classList.toggle('activeGroup');
	saveGroupName.style.display = 'none';

	// формируем список студентов в правом блоке
	table = document.querySelector('.stdList');
	table.innerHTML = '<tr><th>Имя</th><th colspan="2">Активность</th><th></th></tr>';
	groupId = target.dataset.id;
	if (arStdsByGroupIds[groupId]){
		arStdsByGroupIds[groupId].forEach(function(std){
			tr = document.createElement('tr');
			// ячейка имени
			namess = document.createElement('td');
			if (someFigure > 1){
				namess.setAttribute('contenteditable', true);
			}
			namess.innerHTML = std.name;
			namess.dataset.stdId = std.sId;
			namess.title = 'Редактировать имя';
			namess.addEventListener("keydown", function (){
				document.querySelector('.saveStds').style.display = 'inline-block';
				this.dataset.changed = 'true';
			});
			// ячейка активности
			activity = document.createElement('td');
			activity.innerHTML = std.activity + '%';
			if (std.activity < 50){
				if (std.activity < 30) tr.style.color = '#9C0000';
				else tr.style.color = '#FEC005';
			}
			// ячейка удаления
			del = document.createElement('td');
			img = document.createElement('img');
			img.src = '../../img/close.png';
			img.title = 'Удалить';
			del.appendChild(img);

			tr.appendChild(namess);
			tr.appendChild(activity);
			tr.appendChild(del);

			table.appendChild(tr);
		});
	}
	trs = document.querySelectorAll('.stdList tr:not(:first-child)');
	[].forEach.call(trs, function(item){
		item.addEventListener("mouseover", function showDelete(){
			this.lastElementChild.firstElementChild.style.display = 'inline';
		});
		item.addEventListener("mouseout", function hideDelete(){
			this.lastElementChild.firstElementChild.style.display = 'none';

		});
	});
	// удаляем поля для добавления студентов, если они есть
	var stdInputs = document.querySelectorAll('.newStdField');
	[].forEach.call( stdInputs, function(item,i,arr){
		item.remove();
	} );
	var searchField = document.querySelector('.searchStudentField');
	if ( searchField ){
		searchField.remove();
		document.querySelector('.resultList').remove();
	}

};
var generateStudentList, stdUlList, stdList = [], arNewCustomStds = [], searchStdEvent;
(function searchNews() {
	var searchField = document.querySelector('.searchStudentField'),
		resultList = document.querySelector('.resultList'),
		stdName,
		last_name,
		first_name,
		arStdName,
		item,
		student,
		newStd,
		regExp,
		i,
		n = 1;

	generateStudentList = function(stdName,addToEx) {
		arStdName = stdName.split(' ');
		last_name = arStdName[0];
		first_name = '';
		if (arStdName.length > 1) {
			first_name = arStdName[1];
		}
		if (stdList.length) {
			for (i in stdList) {
				if (document.querySelector('.resultItem span[data-id="' + stdList[i].id + '"]')){
					if (stdList[i].last_name.toLowerCase().indexOf(last_name.toLowerCase()) == -1 || arNewCustomStds.indexOf(stdList[i].id) != -1) {
						document.querySelector('.resultItem span[data-id="' + stdList[i].id + '"]').parentNode.style.display = 'none';
					} else {
						regExp = new RegExp('^' + first_name, 'i');
						if (!regExp.test(stdList[i].first_name)) {
							document.querySelector('.resultItem span[data-id="' + stdList[i].id + '"]').parentNode.style.display = 'none';
						} else {
							document.querySelector('.resultItem span[data-id="' + stdList[i].id + '"]').parentNode.style.display = 'block';
						}
					}
				}
			}
		} else {
			$.ajax({
				url: '/project/search.php',
				type: 'POST',
				data: {last_name: last_name},
				success: function (data) {
					stdList = jQuery.parseJSON(data);
					var newStdList,
						resultList = document.querySelector('.resultList'),
						i = 0,
						stdListLength = stdList.length;
					for (; i < stdListLength; i += 1) {
						if (arNewCustomStds.indexOf(stdList[i].id) != -1){
							continue;
						}
						item = document.createElement('div');
						student = document.createElement('span');
						student.dataset.id = stdList[i].id;
						student.innerHTML = stdList[i].last_name + ' ' + stdList[i].first_name + ', ' + stdList[i].group_number;
						item.appendChild(student);
						item.className = 'resultItem';
						resultList.appendChild(item);

						item.addEventListener("click", (function(i,student) {
							return function(){
								arNewCustomStds.push(stdList[i].id);
								if (addToEx){
									var newStdTr = document.createElement('tr');
									var newStdTd = document.createElement('td');
									newStdTd.className = 'newStdTd';
									newStdTd.colSpan = 3;
									newStdTd.innerHTML = stdList[i].last_name + ' ' + stdList[i].first_name;
									newStdTd.dataset.id = stdList[i].id;
									newStdTr.appendChild(newStdTd);
									newStdTr.title = 'Нажмите, чтобы удалить';
									document.querySelector('.rightBlock .stdList').appendChild(newStdTr);
									newStdTr.onclick = function(e){
										arNewCustomStds.splice(arNewCustomStds.indexOf(e.target.dataset.id),1);
										this.remove();
									};
								} else {
									newStd = document.createElement('li');
									newStd.innerHTML = '<span data-id="' + stdList[i].id + '" class="listItem">' + arNewCustomStds.length + '.</span> ' + student.innerHTML;
									stdUlList.appendChild(newStd);
									// добавляем кнопку на сохранение группы
									if (arNewCustomStds.length == 1) {
										var saveNewGroupBtn = document.createElement('a'),
											newGroupNameField,
											newGroupName,
											newStdIds = [],
											newGroup;

										saveNewGroupBtn.innerHTML = 'Сохранить группу';
										saveNewGroupBtn.className = 'saveNewGroup btnBottom';
										rightBlock.appendChild(saveNewGroupBtn);

										saveNewGroupBtn.onclick = function () {
											newGroupNameField = document.querySelector('.editGroupName');
											newGroupName = newGroupNameField.value;
											if (newGroupName.length == 0) {
												alert('Введите название группы.');
												newGroupNameField.focus();
												return;
											}
											newStdList = document.querySelectorAll('.rightBlock .listItem');
											[].forEach.call(newStdList, function (item, j) {
												newStdIds.push(item.dataset.id);
											});
											newGroup = {
												newStdIds: newStdIds,
												customGroupName: newGroupName
											};
											$.ajax({
												url: 'addNewGroup.php',
												type: 'POST',
												data: newGroup,
												success: function () {
													var message = document.createElement('span');
													message.innerHTML = 'Группа "' + newGroupName + '" добавлена';
													message.className = 'msgSuccess';
													message.style.marginLeft = saveNewGroupBtn.offsetWidth + 10 + 'px';
													rightBlock.appendChild(message);
													setTimeout(function () {
														location.reload();
													}, 1000);
												}
											})
										}
									}
									newStd.onclick = function (e) {
										targetChild = e.target.firstChild;
										arNewCustomStds.splice(arNewCustomStds.indexOf(targetChild.dataset.id),1);
										this.remove();
										newStdList = document.querySelectorAll('.rightBlock .listItem');
										[].forEach.call(newStdList, function (item, z) {
											z++;
											item.innerHTML = z + '.';
										});
										if (newStdList.length == 0)
											document.querySelector('.rightBlock .saveNewGroup').remove();

									};
								}
								this.style.display = 'none';
							}
						})(i,student))
					}
				}
			})
		}
	};
	searchStdEvent = function(searchField,resultList,addToEx){
		var stdNamePrevLength = 100,
			stdLiItems,
			searchNews__hint = document.querySelector('.searchNews__hint'),
			timer;
		searchField.onkeyup = function(){
			if (stdNamePrevLength == 100 && searchNews__hint){
				searchNews__hint.style.opacity = 0;
				timer = getComputedStyle(searchNews__hint)['transitionDuration'];
				timer = timer.substring(0, timer.length - 1);
				setTimeout(function(){
					searchNews__hint.style.display = 'none';
				}, timer *1000);
			}
			stdName = this.value.trim();
			if (stdName.length > 2){
				stdLiItems = document.querySelectorAll('.rightBlock .listItem');
				generateStudentList(stdName,addToEx);
			} else if (stdName.length == 0){
				resultList.innerHTML = '';
				stdList = '';
			}
			stdNamePrevLength = stdName.length;
		}
	}
})();

addGroup = document.querySelector('.newGroup');
if (addGroup){
	addGroup.onclick = function(){
		rightBlock.innerHTML = '';
		if (document.querySelector('.activeGroup')){
			document.querySelector('.activeGroup').classList.remove('activeGroup');
		}
		this.style.display = 'none';
		document.querySelector('.editGroup .action').style.display = 'none';
		saveGroupName.style.display = 'none';

		span = document.createElement('span');
		inputOne = document.createElement('input');

		a = document.createElement('a');
		a.className = 'saveNewGroup';
		a.innerHTML = 'OK';
		a.href = '#';
		groupType = document.createElement('input');
		groupType.type = 'checkbox';
		groupType.checked = true;
		groupTypeDesc = document.createElement('label');
		groupTypeDesc.innerHTML = 'Учебная';
		groupTypeDesc.appendChild(groupType);

		inputOne.className = 'editGroupSize';
		span.innerHTML = 'Кол-во студентов:';
		var hintGroupType = document.createElement('div');
		hintGroupType.className = 'hintGroupType';
		hintGroupType.innerHTML = 'Если не отмечено <span>"Учебная"</span>, то будет предложено сформировать группу из существующего списка студентов';
		var groupInfo = document.querySelector('.groupInfo');
		groupInfo.appendChild(groupTypeDesc);
		var br1 = document.createElement('br');
		var br2 = document.createElement('br');
		groupInfo.appendChild(br1);
		groupInfo.appendChild(br2);
		groupInfo.appendChild(span);
		groupInfo.appendChild(inputOne);
		groupInfo.appendChild(a);
		document.querySelector('.editGroup').appendChild(hintGroupType);
		if (someFigure < 2){
			groupTypeDesc.style.display = 'none';
			inputOne.style.display = 'none';
			hintGroupType.style.display = 'none';
			span.style.display = 'none';
			br1.style.display = 'none';
			br2.style.display = 'none';
			groupType.checked = false;
		}

		groupType.onchange = function(){
			if (!this.checked){
				span.remove();
				inputOne.remove();
				br2.remove();
				br1.remove();
			} else{
				groupInfo.appendChild(br1);
				groupInfo.appendChild(br2);
				groupInfo.appendChild(span);
				groupInfo.appendChild(inputOne);
			}
		};

		addGroup.className = 'group newGroup';
		input = document.createElement('input');
		input.type = 'button';
		input.className = 'group activeGroup newGroupLabel';
		input.value = 'name';
		document.querySelector('.groups').insertBefore(input, addGroup);

		groupNameField.value = '';
		groupNameField.dataset.action = 'edit';

		groupNameField.focus();

		groupNameField.onkeyup = function(){
			input.value = this.value;
		};

		document.querySelector('.saveNewGroup').onclick = function(){
			var newGroupNameField = document.querySelector('.editGroupName');
			var newGroupName = newGroupNameField.value;
			if (newGroupName.length == 0){
				newGroupNameField.style.transition = '.5s';
				newGroupNameField.style.backgroundColor = '#FF2E2E';
				setTimeout( function(){
					newGroupNameField.style.backgroundColor = '#FFFFFF';
				}, 2000 );
			}
			var groupType = document.querySelector('.groupInfo input[type="checkbox"]');
			if (groupType.checked){
				rightBlock.innerHTML = '';
				count = document.querySelector('.editGroupSize').value;
				div = document.createElement('div');
				div.className = 'newStdList';
				var input, i = 0;
				for (; i<count; i += 1){
					input = document.createElement('input');
					if (i==0){
						input.placeholder = 'Фамилия Имя';
					}
					input.type = 'text';
					input.className = 'newStds';
					div.appendChild( input );
				}
				rightBlock.appendChild(div);

				addNewGroup = document.createElement('a');
				addNewGroup.className = 'saveNewGroup btnBottom';
				addNewGroup.innerHTML = 'Сохранить группу';
				rightBlock.appendChild(addNewGroup);

				addNewGroup.onclick = function(){
					// Проверка названия группы
					newGroupNameField = document.querySelector('.editGroupName');
					newGroupName = newGroupNameField.value;
					if (newGroupName.length == 0){
						alert('Введите название группы.');
						newGroupNameField.focus();
						return;
					}
					var stds = document.querySelectorAll('.newStdList input');
					var newStdList = {};
					[].forEach.call( stds, function(item,i){
						if (item.value.length < 2) return true;
						var name = item.value.split(' ');
						newStdList[i] = {};
						newStdList[i]['lastName'] = name[0];
						newStdList[i]['firstName'] = name[1];

					} );
					var newGroup = {
						stdList: newStdList,
						groupName: newGroupName
					};
					$.ajax({
						url: 'addNewGroup.php',
						type: 'POST',
						data: newGroup,
						success: function(){
							var message = document.createElement('span');
							message.innerHTML = 'Группа "' + newGroupName + '" добавлена';
							message.className = 'msgSuccess';
							rightBlock.appendChild(message);

							setTimeout( function(){
								location.reload();
							}, 1000 );
						}
					})
				}
			} else {
				rightBlock.innerHTML = '<p class="rightBlockTitle">Формируем группу из общего списка:</p>';
				var searchStudentField = document.createElement('input'),
					resultStudentsList = document.createElement('div'),
					newGroupStdList = document.createElement('div'),
					searchHint = document.createElement('span');

				stdUlList = document.createElement('ul');
				searchHint.className = 'searchNews__hint';
				searchHint.innerHTML = 'Начните вводить фамилию';
				newGroupStdList.appendChild(stdUlList);
				resultStudentsList.className = 'resultList';
				searchStudentField.className = 'searchStudentField';
				searchStudentField.type = 'text';
				searchStudentField.placeholder = 'Фамилия Имя';
				rightBlock.appendChild(newGroupStdList);
				rightBlock.appendChild(searchStudentField);
				rightBlock.appendChild(searchHint);
				rightBlock.appendChild(resultStudentsList);

				// поиск студентов
				var searchField = document.querySelector('.searchStudentField');
				var resultList = document.querySelector('.resultList');
				if (searchField){
					searchStdEvent(searchField,resultList);
				}
			}
			return false;
		}
	}
}

groupNameField.onkeydown = function(){
	if (!groupNameField.dataset.action){
		saveGroupName.style.display = 'inline';
	}
};
var container = document.querySelector('.container');
function deleteStd(e){
	var target = e.target;
	if (target.tagName == 'IMG'){
		var curTr = target.parentNode.parentNode;
		var stdId = curTr.firstElementChild.dataset.stdId;
		data = {stdId: stdId};
		if (document.querySelector('.activeGroup').dataset.custom == 1){
			data.custom = true;
			data.gId = document.querySelector('.activeGroup').dataset.id;
		}
		$.ajax({
			url: 'delete.php',
			type: 'POST',
			data: data,
			success: function(){
				if (!document.querySelector('.warning')){
					var warning = document.createElement('p');
					warning.className = 'warning';
					// var aReloadP = document.createElement('a');
					// aReloadP.innerHTML = 'Обновите страницу';
					// aReloadP.onclick = function(){
					// 	location.reload();
					// };
					warning.innerHTML = 'Данные изменены. <span onclick="location.reload()">Обновите страницу</span> для корректного отображения измененных данных.';
					container.insertBefore(warning,document.querySelector('.leftBlock'));
				}
				curTr.remove();
			}
		})
	}
}
// удалние студента при нажатии на крестик
if ( document.querySelector('.stdList') ){
	document.querySelector('.stdList').onclick = function(e){
		deleteStd(e);
	}
}

function saveNewStds(is_cusom){
	var stdInputs,
		name,
		groupId = document.querySelector('.activeGroup').dataset.id,
		changedStds = document.querySelectorAll('.stdList [data-changed="true"]'),
		students = {};
	if (is_cusom){
		stdInputs = document.querySelectorAll('.newStdTd');
	} else {
		stdInputs = document.querySelectorAll('.newStdField');
	}

	if (changedStds.length > 0){
		var stdList = {};
		[].forEach.call( changedStds, function(item,i){
			name = item.innerHTML.split(' ');
			stdList[i] = {};
			stdList[i]['lastName'] = name[0];
			stdList[i]['firstName'] = name[1];
			stdList[i]['id'] = item.dataset.stdId;
		} );
		students.stdList = stdList;

	}
	if (stdInputs.length > 0){
		var newStds = {};
		[].forEach.call( stdInputs, function(item,i){
			newStds[i] = {};
			if (is_cusom){
				name = item.innerHTML.trim();
				newStds[i]['id'] = item.dataset.id;
			} else {
				name = item.value.trim();
			}
			name = name.split(' ');
			newStds[i]['lastName'] = name[0];
			newStds[i]['firstName'] = name[1];
		} );
		students.newStds = newStds;
		students.groupNumber = groupId;
	}
	$.ajax({
		url: 'stds.php',
		type: 'POST',
		data: students,
		success: function(){
			var msgStdsSaved = document.createElement('span');
			msgStdsSaved.innerHTML = 'Изменения сохранены';
			msgStdsSaved.className = 'msgSuccess';
			document.querySelector('.editStds').appendChild(msgStdsSaved);
			setTimeout( function(){
				msgStdsSaved.style.opacity = 0;
				location.reload();
			}, 700 );
		}
	});
}
if ( document.querySelector('.saveStds') ){
		saveStds = function (){
			if (document.querySelector('.activeGroup').dataset.custom == 1){
				saveNewStds(true);
			} else {
				saveNewStds(false);
			}
			return false;
		};
		document.querySelector('.saveStds').onclick = saveStds;
}

if ( document.querySelector('.addStd') ){
	document.querySelector('.addStd').onclick = addStdToTable;
	function addStdToTable(){
		document.querySelector('.saveStds').style.display = 'inline-block';
		var newStdInput = document.createElement('input');
		newStdInput.placeholder = 'Фамилия Имя';
		if (document.querySelector('.activeGroup').dataset.custom == 1){
			document.querySelector('.addStd').style.display = 'none';
			newStdInput.className = 'searchStudentField';
			resultList = document.createElement('div');
			resultList.className = 'resultList';
			rightBlock.insertBefore(newStdInput,document.querySelector('.editStds'));
			rightBlock.insertBefore(resultList,document.querySelector('.editStds'));
			searchStdEvent(newStdInput,resultList,true);
		} else {
			newStdInput.className = 'newStdField';
			rightBlock.insertBefore(newStdInput,document.querySelector('.editStds'));
		}
		newStdInput.focus();
		return false;
	}
}

saveGroupName.onclick = function(){
	var groupNumber = document.querySelector('.activeGroup').dataset.id;
	var newGroupName = groupNameField.value;

	var groupInfo = {
		groupNumber: groupNumber,
		newGroupName: newGroupName
	};
	$.ajax({
		url: 'addNewGroup.php',
		type: 'POST',
		data: groupInfo,
		success: function(data){
			msg = document.createElement('span');
			msg.innerHTML = 'Название обновлено';
			msg.className = 'msgSuccess';
			document.querySelector('.groupInfo').appendChild(msg);
			setTimeout( function(){
				saveGroupName.style.display = 'none';
				msg.remove();
			}, 2000 );
			document.querySelector('.activeGroup').value = newGroupName;
		}
	});
	return false;
};
rightBlock.onclick = function(e){
	if ( e.target.className == 'resultItem' || e.target.className == 'searchStudentField'
			|| e.target.tagName == 'SPAN' || e.target.tagName == 'LI') return;
	resultList = document.querySelector('.resultList');
	if ( resultList ){
		resultList.innerHTML = '';
		stdList = '';
	}

};

