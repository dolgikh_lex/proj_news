(function searchNews(){
	var searchField = document.querySelector('input[name="searchField"]'),
		resultList = document.querySelector('.resultList'),
		stdName,
		last_name,
		first_name,
		arStdName,
		item,
		link,
		stdList,
		regExp,
		i;

	function generateStudentList(stdName){
		arStdName = stdName.split(' ');
		last_name = arStdName[0];
		first_name = '';
		if (arStdName.length > 1){
			first_name = arStdName[1];
		}
		if (stdList){
			for (i in stdList){
				if ( stdList[i].last_name.toLowerCase().indexOf(last_name.toLowerCase() ) == -1){
					document.querySelector('.resultItem[data-id="'+stdList[i].id+'"]').style.display = 'none';
				} else {
					regExp = new RegExp('^'+first_name, 'i');
					if ( !regExp.test(stdList[i].first_name) ){
						document.querySelector('.resultItem[data-id="'+stdList[i].id+'"]').style.display = 'none';
					} else {
						document.querySelector('.resultItem[data-id="'+stdList[i].id+'"]').style.display = 'block';
					}
				}
			}
		} else {
			$.ajax({
				url: 'search.php',
				type: 'POST',
				data: {last_name: last_name},
				success: function(data){
					stdList = jQuery.parseJSON(data);
					for (i in stdList){
						item = document.createElement('div');
						link = document.createElement('a');
						link.href = '?id='+stdList[i].id;
						link.innerHTML = stdList[i].last_name + ' ' + stdList[i].first_name + ', ' + stdList[i].group_number;
						item.appendChild(link);
						item.className = 'resultItem';
						item.dataset.id = stdList[i].id;
						resultList.appendChild(item);
					}
				}
			})
		}
	}

	if (searchField){
		var stdNamePrevLength = 100,
			searchNews__hint = document.querySelector('.searchNews__hint'),
			timer;
		searchField.onkeyup = function(){
			if (stdNamePrevLength == 100){
				searchNews__hint.style.opacity = 0;
				timer = getComputedStyle(searchNews__hint)['transitionDuration'];
				timer = timer.substring(0, timer.length - 1);
				setTimeout(function(){
					searchNews__hint.style.display = 'none';
				}, timer *1000);
			}
			stdName = this.value.trim();
			var Reg = new RegExp("^[A-zА-яЁё ]+$");
			if (stdName.length > 1 && Reg.test(stdName)){
				generateStudentList(stdName);
			} else if (stdName.length == 0){
				resultList.innerHTML = '';
				stdList = '';
			}
			stdNamePrevLength = stdName.length;
		}
	}
})();
