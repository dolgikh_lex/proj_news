<?php
    if ( isset($_POST['last_name']) ){
        session_start();
        include('dbconnect.php');
        $last_name = mysqli_real_escape_string($connect,$_POST['last_name']);
        $first_name = mysqli_real_escape_string($connect,$_POST['first_name']);
        $patronymic = mysqli_real_escape_string($connect,$_POST['patronymic']);
        $password = $_POST['password'];

        if ($stmt = mysqli_prepare($connect, "SELECT tchr_id, tchr_last_name, tchr_first_name, tchr_patronymic, password, tchr_level 
          FROM teachers WHERE tchr_last_name = ? AND tchr_first_name = ? AND tchr_patronymic = ?")) {
            mysqli_stmt_bind_param($stmt,"sss", $last_name,$first_name,$patronymic);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $id,$last_name,$first_name,$patronymic,$password,$tchr_level);
            if ( mysqli_stmt_fetch($stmt) ){
                if (!password_verify($_POST['password'], $password)) {
                    echo 'Неверные данные!';
                    die();
                }
                $_SESSION['user']['first_name'] = $first_name;
                $_SESSION['user']['last_name'] = $last_name;
                $_SESSION['user']['patronymic'] = $patronymic;
                $_SESSION['user']['level'] = $tchr_level;
                $_SESSION['user']['id'] = $id;
            } else {
                    echo 'Неверные данные!';
            }
            mysqli_stmt_close($stmt);
            mysqli_close($connect);
        }
    } else {
        die();
    }

?>