<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/project/doc_header.php')
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Добавить новость</title>
	<link rel="stylesheet" href="/project/css/main.css">
	<link rel="stylesheet" href="admin.css">
	<script src="/project/js/jquery-1.12.3.min.js"></script>
</head>
<body>	
	<?php
		if ($_SESSION['user']){
			$last_name = $_SESSION['user']['last_name'];
			$first_name = substr($_SESSION['user']['first_name'], 0, 2);
			$patronymic = substr($_SESSION['user']['patronymic'], 0, 2);
		}
	?>
	<div class="wrapper">
		<div class="mainContent">
			<div class="header">
				<div class="toolbar">
					<div class="contentWidth">
						<div class="loginName">
							<span class='userName'><?= "$last_name $first_name. $patronymic." ?></span>
							<ul class='navMenu'>
								<li class='active'><a href='/project/admin'>Добавить новость</a></li>
								<li><a href='/project/admin/news/'>Список новостей</a></li>
								<li><a href='/project/admin/groups'>Управление группами</a></li>
								<?php
									$last_item_menu_text = 'Профиль';
									$last_item_menu_url = 'profile';
									if ( $_SESSION['user']['level'] == 3 ){
										$last_item_menu_text = 'Администраторы';
										$last_item_menu_url = 'admin-list';
									} 
								?>
								<li class='nav-admins'><?= "<a href='/project/admin/$last_item_menu_url'>$last_item_menu_text</a>" ?></li>
							</ul>
						</div>
						<div class="log-in-out">
							<span class='logOut'><a href='/project/admin/?out'> Выйти </a></span>
						</div>
					</div>				
				</div>
			</div>
			<div class="content">
				<div class="contentWidth">
					<div class="breadCrumbs">
						<a href="/project/">Главная</a> / Панель администратора
					</div>
					<h2>Добавление новости</h2>		
					<div class="addNew">
						<form method="POST" enctype="multipart/form-data">
							<div class="formContent">
								<div class="leftSide">
									<input name="heading" type="text" placeholder="Заголовок">
									<textarea name="text" id="" cols="30" rows="10" placeholder="Текст"></textarea>
									<input name="date" type="text">
										<div class="heading">
											Прикрепить файлы
											<img src="../img/Triangle.png">
										</div>
										<div class="files">
											<div class="file">
												<label for="newImg">Картинка к новости</label>
												<input name="newImg" id="newImg" type="file">
											</div>
											<div class="file">
												<label for="newFile">Прикрепленные файлы</label>
												<input name="newFiles[]" id="newFile" type="file" multiple="true">
											</div>
										</div>
								</div>
								<div class="rightSide">
									<div class="forGroup">
										<?php
											$query_groups = "SELECT group_id, name FROM groups WHERE is_custom = 0 
																OR (is_custom = 1 AND creater_id = '{$_SESSION['user']['id']}') ORDER BY name";
											$result = mysqli_query($connect, $query_groups);
										?>
										<div class="heading openHeading">
											Группы
											<img src="../img/Triangle.png">
										</div>
										<div class="forContent" style="display: block;">
											<div class="selectAll">
													<a href="#" class="all">Выделить всё</a>
													<a href="#" class="nothing">Снять выделение</a>
											</div>
											<div class="listItems">
												<?php
													while( $row = mysqli_fetch_array($result) ){
														echo "<label class='groupName'>{$row['name']}<input name='groupList[]' type='checkbox' value='{$row['name']}' data-id='{$row['group_id']}' hidden></label>";
													}
												?>
											</div>
											
										</div>
									</div>
									<div class="forStudent">
										<div class="heading">
											Студенты
											<img src="../img/Triangle.png">
										</div>
										<div class="forContent">
											<div class="selectAll">
												<a href="#" class="all">Выделить всё</a>
												<a href="#" class="nothing">Снять выделение</a>
											</div>
											<div class="listItems">
											</div>
										</div>
									</div>
								</div>
							</div>	
							<input type="text" name="author" value="<?=$_SESSION['user']['last_name'].' '.$_SESSION['user']['first_name'].' '.$_SESSION['user']['patronymic']?>" hidden>
							<div class="submitBlock">
								<input class="inactive" type="submit" value="Добавить новость">
								<img class="successImg" src="../img/successSend.png" alt="">
							</div>								
						</form>
					</div>	
				</div>
			</div>
		</div>
		<?php require_once('footer.php');?>
	</div>
	<script src="../js/addNew.min.js"></script>
</body>
</html>