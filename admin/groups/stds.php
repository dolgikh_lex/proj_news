<?php
	include('../../dbconnect.php');
	$connect = mysqli_connect(host,user,password,dbname);
	mysqli_query($connect, "SET NAMES utf8");
	if ( isset($_POST['groupId']) ){
		$students = array();
		$groupName = $_POST['groupId'];
		$query = "SELECT * FROM students s LEFT JOIN std_group sg ON s.std_id = sg.std_id  WHERE sg.group_id = '$groupName' ORDER BY s.last_name";
		$result = mysqli_query($connect, $query);

		$i = 0;
		while( $row = mysqli_fetch_assoc($result) ){
			// Считаем активность для студента
			$query1 = "SELECT * FROM new_receiver WHERE std_id='{$row['std_id']}'";
			$result1 = mysqli_query($connect, $query1);
			$allNews = mysqli_num_rows($result1);
			$checkedNews = 0;
			while ( $row1 = mysqli_fetch_assoc($result1) ){
				if ($row1['read_status'] == 2) $checkedNews+=1;
			}
			if ($allNews == 0){
				$activatyPerc = 0;
			} else{
				$activatyPerc = round($checkedNews * 100 / $allNews, 1);
			}
			$students[$i] = [];
			$students[$i]['name'] = $row['last_name'].' '.$row['first_name'];
			$students[$i]['id'] = $row['std_id'];
			$students[$i]['activaty'] = $activatyPerc;
			$i+=1;
		}
			echo json_encode($students);
	} 

	if ( isset($_POST['stdList']) ) {
		$stdList = $_POST['stdList'];
		for ( $i=0; $i<count($stdList); $i++ ){
			$last_name = $stdList[$i]['lastName'];
			$first_name = $stdList[$i]['firstName'];
			$std_id = $stdList[$i]['id'];
			$query = "UPDATE students SET last_name='$last_name',first_name='$first_name' WHERE std_id='$std_id'";
			mysqli_query($connect,$query);
		}
	}

	if ( isset($_POST['newStds']) ) {
		$stdList = $_POST['newStds'];
		$group = $_POST['groupNumber'];
		for ( $i=0; $i<count($stdList); $i++ ){
			$last_name = $stdList[$i]['lastName'];
			$first_name = $stdList[$i]['firstName'];
            if ( !isset($stdList[$i]['id']) ){
                $query = "INSERT INTO students VALUES(null,'$first_name','$last_name')";
                mysqli_query($connect,$query);
                $std_id = mysqli_insert_id($connect);
            } else {
                $std_id = $stdList[$i]['id'];
            }
			$query2 = "INSERT INTO std_group VALUES(null,'$std_id','$group')";
			mysqli_query($connect,$query2);
		}
	}
?>