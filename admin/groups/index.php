<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/project/doc_header.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Управление группами</title>
	<link rel="stylesheet" href="/project/css/main.css">
	<link rel="stylesheet" href="/project/admin/admin.css">
	<link rel="stylesheet" href="/project/admin/groups/groups.css">
	<script src="/project/js/jquery-1.12.3.min.js"></script>
</head>
<body>	
	<?php
		if ($_SESSION['user']){
			$last_name = $_SESSION['user']['last_name'];
			$first_name = substr($_SESSION['user']['first_name'], 0, 2);
			$patronymic = substr($_SESSION['user']['patronymic'], 0, 2);
		}
	?>
	<div class="wrapper">
		<div class="mainContent">
			<div class="header">
				<div class="toolbar">
					<div class="contentWidth">
						<div class="loginName">
							<span class='userName'><?= "$last_name $first_name. $patronymic." ?></span>
							<ul class='navMenu'>
								<li><a href='/project/admin/'>Добавить новость</a></li>
								<li><a href='/project/admin/news'>Список новостей</a></li>
								<li class='active'><a href='/project/admin/groups/'>Управление группами</a></li>
								<?php
									$last_item_menu_text = 'Профиль';
									$last_item_menu_url = 'profile';
									if ( $_SESSION['user']['level'] == 3 ){
										$last_item_menu_text = 'Администраторы';
										$last_item_menu_url = 'admin-list';
									}
								?>
								<li class='nav-admins'><?= "<a href='/project/admin/$last_item_menu_url'>$last_item_menu_text</a>" ?></li>
							</ul>
						</div>
						<div class="log-in-out">
							<span class='logOut'><a href='/project/?out'> Выйти </a></span>
						</div>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="contentWidth">
					<div class="breadCrumbs">
						<a href="/project">Главная</a> / Управление групами
					</div>
					<h3>Список групп</h3>
					<div class="container">
						<div class="leftBlock">
							<div class="groups">
								<?php
								$access = true;
								if ($_SESSION['user']['level'] < 2){ // если ур1, то выдаем только его кастомные группы
									$query = "SELECT * FROM groups WHERE is_custom = 1
 													AND creater_id = '{$_SESSION['user']['id']}' ORDER BY name";
								} else {
									$query = "SELECT * FROM groups WHERE is_custom = 0 OR (is_custom = 1 
												AND creater_id = '{$_SESSION['user']['id']}') ORDER BY name";
								}
								$result = mysqli_query($connect,$query);
								if (mysqli_num_rows($result) == 0){
									$access = false; ?>
									<p>Вы еще не создали своей группы</p>
								<?php } else {
									$defGroup = 0;
									while ($row = mysqli_fetch_array($result)) {
										echo "<input data-id='{$row['group_id']}' data-custom='{$row['is_custom']}' class='group";
										if ($defGroup === 0) {
											echo " activeGroup";
											$defGroup = $row['name'];
											$defGroupId = $row['group_id'];
										}
										echo "' type='button' value='{$row['name']}'>";
									}
								} ?>
								<input class='group newGroup' type='button' value='+' title="Добавить группу">
							</div>
							<div class="editGroup">
								<div class="groupInfo">
									<span>Название:</span>
									<input class="editGroupName" type="text" value='<?=$defGroup?>'>
									<a class="saveGroupName" href="">Сохранить</a>

								</div>

								<div class="action">
									<a <?php echo !$access ? 'style="display: none;"' : ''?> class="deleteGroup" href="">Удалить группу</a>
								</div>


							</div>
						</div>
						<div class="rightBlock">
							<?php if ($access) { ?>
							<table class="stdList">
								<tr>
									<th>Имя</th>
									<th colspan="2">Активность</th>
									<th></th>
								</tr>
								<?php
								$query = "SELECT * FROM students s LEFT JOIN std_group sg ON s.std_id = sg.std_id  WHERE sg.group_id = '$defGroupId' ORDER BY s.last_name";
								$result = mysqli_query($connect, $query);
								while ( $row = mysqli_fetch_assoc($result) ){
									// считаем активность для каждого студента
									$query1 = "SELECT * FROM new_receiver WHERE std_id='{$row['std_id']}'";
									$result1 = mysqli_query($connect, $query1);
									$allNews = mysqli_num_rows($result1);
									$checkedNews = 0;
									while ( $row1 = mysqli_fetch_assoc($result1) ){
										if ($row1['read_status'] == 2) $checkedNews+=1;
									}
									if ($allNews == 0) $activatyPerc = 0;
									else $activatyPerc = round($checkedNews * 100 / $allNews, 1);

									$actColor = 'black';
									if ($activatyPerc < 50){
										if ($activatyPerc < 20) $actColor = '#9C0000';
										else $actColor = '#FEC005';
									} ?>

									<tr style='color: <?=$actColor;?>'>
										<td <?php echo $_SESSION['user']['level'] > 1 ? 'contenteditable' : ''?> title='Редактировать имя' data-std-id="<?=$row['std_id']?>"><?=$row['last_name'].' '.$row['first_name']?></td>
										<td><?=$activatyPerc?>%</td>
										<td><img src='../../img/close.png' alt='' title='Удалить'></td>
									</tr>
								<?php }	?>
							</table>
							<div class="editStds">
								<a class="addStd" href="">Добавить студента</a>
								<a class="saveStds" href="">Сохранить</a>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php require_once('../footer.php');?>
	</div>
	<script src="../../js/groups.min.js"></script>
	<script>
		<?php echo "var someFigure = ".$_SESSION['user']['level'].";";?>
	</script>
</body>
</html>