<?php
	include('../dbconnect.php');

	// Получаем id выбранных студентов
	$stdList = $_POST['students'];
	$stdList = json_decode($stdList, true);

	// Загаловок, текст ... новости
	$heading = htmlentities( mysqli_real_escape_string($connect,$_POST['heading']) );
	$text = htmlentities( mysqli_real_escape_string($connect,$_POST['text']) );
	$date = htmlentities( mysqli_real_escape_string($connect,$_POST['date']) );
	$author = htmlentities( mysqli_real_escape_string($connect,$_POST['author']) );
    $author = explode(' ',$author);
    $authorLName = $author[0];
    $authorFName = $author[1];
    $authorPatronymic = $author[2];
	$img = $_FILES['newImg'];
	$files = $_FILES['newFiles'];

	// Запрос на выборку автора добавленной новости
	$query = "SELECT tchr_id FROM teachers WHERE tchr_last_name LIKE '$authorLName' 
                AND tchr_first_name LIKE '$authorFName' AND tchr_patronymic LIKE '$authorPatronymic'";
	$result = mysqli_query($connect, $query);
	$athor_id = mysqli_fetch_row($result);
	$athor_id = $athor_id[0];

	// загрузка картинки
	if ($img['name']){
		$uploaddirImg = $_SERVER['DOCUMENT_ROOT'].'/project/files/newsImg/';
		$imgName = $img['name'];
		$decodeImgName = iconv("UTF-8", "windows-1251",$imgName);
		$imgParts = explode("\.", $imgName);
		$imgExt = $imgParts[count($imgParts)-1];
		$uniqImgName = uniqid().'.'.$imgExt;
		$uploadImg = $uploaddirImg.$uniqImgName;
		copy($img['tmp_name'], $uploadImg);
		$query2 = "INSERT INTO news(heading, text, pub_date, author, img) VALUES('$heading', '$text', '$date', '$athor_id', '$uploadImg')";
	} else{
		$query2 = "INSERT INTO news(heading, text, pub_date, author) VALUES('$heading', '$text', '$date', '$athor_id')";
	}

	// Добавление новости
	mysqli_query($connect, $query2) or die( mysqli_error($connect) );
	$new_id = mysqli_insert_id($connect);

	// Загрузка файлов
	if ($files['name'][0]){
		$uploaddirFiles = $_SERVER['DOCUMENT_ROOT'].'/project/files/files/';
		for ($i = 0; $i < count($files['name']); $i++){
			$fileName = $files['name'][$i];
			// $decodeFileName = iconv("UTF-8", "windows-1251",$fileName);
			$fileParts = explode("\.", $fileName);
			$fileExt = $fileParts[count($fileParts)-1];
			$uniqFileName = uniqid().'.'.$fileExt;
			$uploadFile = $uploaddirFiles.$uniqFileName;
			copy($files['tmp_name'][$i], $uploadFile);

			$query4 = "INSERT INTO files(new_id, file_name, file_src) VALUES('$new_id', '$fileName', '$uploadFile')";
			mysqli_query($connect, $query4);
		}
	}
	// Добавление адресатов для новости
	foreach ($stdList as $key => $sId) {
		$query3 = "INSERT INTO new_receiver(new_id,std_id) VALUES ('$new_id', '$sId')";
		mysqli_query($connect,$query3);
	}
?>