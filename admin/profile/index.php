<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/project/doc_header.php')
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Профиль</title>
	<link rel="stylesheet" href="/project/css/main.css">
	<link rel="stylesheet" href="/project/admin/admin.css">
	<link rel="stylesheet" href="/project/admin/admin-list/admin-list.css">
	<script src="/project/js/jquery-1.12.3.min.js"></script>
</head>
<body>	
	<?php
		if ($_SESSION['user']){
			$last_name = $_SESSION['user']['last_name'];
			$first_name = substr($_SESSION['user']['first_name'], 0, 2);
			$patronymic = substr($_SESSION['user']['patronymic'], 0, 2);
		}
	?>
	<div class="wrapper">
		<div class="mainContent">
			<div class="header">
				<div class="toolbar">
					<div class="contentWidth">
						<div class="loginName">
							<span class='userName'><?= "$last_name $first_name. $patronymic." ?></span>
							<ul class='navMenu'>
								<li><a href='/project/admin/'>Добавить новость</a></li>
								<li><a href='/project/admin/news'>Список новостей</a></li>
								<li><a href='/project/admin/groups'>Управление группами</a></li>
								<li class='active nav-admins'><a href='/project/admin/profile/'>Профиль</a></li>
							</ul>
						</div>
						<div class="log-in-out">
							<span class='logOut'><a href='/project/?out'> Выйти </a></span>
						</div>
					</div>				
				</div>
			</div>
			<div class="content">
				<div class="contentWidth">
					<div class="breadCrumbs">
						<a href="/project/">Главная</a> / Профиль
					</div>
					<div class="profile">
						<form class="profile__form" enctype="multipart/form-data" method="POST" action="../admin-list/admins.php">
							<?php
							$tchr_id = $_SESSION['user']['id'];
							$query = "SELECT * FROM teachers WHERE tchr_id = '$tchr_id'";
							$result = mysqli_query($connect,$query);
							$row = mysqli_fetch_assoc($result);

							if ($row['tchr_ava_src'])
								$src = $row['tchr_ava_src'];
							else
								$src = '/project/img/def-ava.gif';?>
							<div class="prof-photo">
								<img src=<?=$src?>>
								<input type="file" id="ava" name="ava">
								<label for="ava">
									<span>изменить</span>
								</label>
							</div>
							<div class="prof-data">
								<input class="prof-data__login" type="text" value="<?= $row['tchr_last_name'].' '.$row['tchr_first_name'].' '.$row['tchr_patronymic'] ?>" name="login" required><span class="label">Логин</span>
								<br>
								<input type="text" value="<?php echo $row['tchr_email'] ? $row['tchr_email'] : '' ?>" name="email"><span class="label">Email</span>
								<br>
								<input type="submit" class="prof-edit-but" value="Сохранить изменения">
							</div>
						</form>
					</div>				
				</div>
			</div>
		</div>
		<?php require_once('../footer.php');?>
	</div>
	<script src="/project/admin/admin-list/admin-list.js"></script>
</body>
</html>