<?php
    session_start();
    include('../../dbconnect.php');
	if ( isset($_POST['lastName']) ){
		$last_name = $_POST['lastName'];
		$first_name = $_POST['firstName'];
		$patronymic = $_POST['patronymic'];
        $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
		$email = $_POST['email'];
		$level = $_POST['level'];
		$query = "INSERT INTO teachers(tchr_first_name, tchr_last_name, tchr_patronymic, password, tchr_level) VALUES('$first_name', '$last_name', '$patronymic', '$password','$level')";
		$result = mysqli_query($connect, $query);
	}

	if ( isset($_POST['adminId']) ){
		$admin_id = $_POST['adminId'];
		$query = "DELETE FROM teachers WHERE tchr_id = '$admin_id'";
		mysqli_query($connect, $query);
	}

	if ( isset($_FILES['ava']) ){
		$id = $_SESSION['user']['id'];
		$login = $_POST['login'];
		$arLogin = explode(" ", $login);
		$first_name = $arLogin[1];
		$last_name = $arLogin[0];
		$patronymic = $arLogin[2];
        $email = $_POST['email'];
		$ava = $_FILES['ava'];
		if ($ava['name']){
			// удаляем текущую аву
			$query1 = "SELECT tchr_ava_src FROM teachers WHERE tchr_id = '$id'";
			$result = mysqli_query($connect, $query1);
			$prevAva = mysqli_fetch_assoc($result);
			unlink($prevAva['tchr_ava_src']);

			$uploaddirImg = $_SERVER['DOCUMENT_ROOT'].'/project/files/tchr_avas/';
			$imgName = $ava['name'];
			$imgParts = explode("\.", $imgName);
			$imgExt = $imgParts[count($imgParts)-1];
			$uniqImgName = uniqid().'.'.$imgExt;
			$uploadImg = $uploaddirImg.$uniqImgName;
			copy($ava['tmp_name'], $uploadImg);
			$query = "UPDATE teachers SET tchr_first_name = '$first_name', tchr_last_name = '$last_name', tchr_patronymic = '$patronymic', tchr_ava_src = '$uploadImg', tchr_email = '$email' WHERE tchr_id = '$id'";
		} else{
			$query = "UPDATE teachers SET tchr_first_name = '$first_name', tchr_last_name = '$last_name', tchr_patronymic = '$patronymic', tchr_email = '$email' WHERE tchr_id = '$id'";
		}
		mysqli_query($connect, $query);
		header("Location: ./");
	}

	if ( isset($_POST['adminEdit']) ){
		$id = $_POST['adminEdit']['id'];
		$login = $_POST['adminEdit']['login'];
		$arLogin = explode(" ", $login);
        $email = $_POST['adminEdit']['email'];
		$first_name = $arLogin[1];
		$last_name = $arLogin[0];
		$patronymic = $arLogin[2];
		$level = $_POST['adminEdit']['level'];
		if ( $_POST['adminEdit']['password'] ){
            $password = password_hash($_POST['adminEdit']['password'], PASSWORD_DEFAULT);
			$query = "UPDATE teachers SET tchr_first_name = '$first_name', tchr_last_name = '$last_name', 
              tchr_patronymic = '$patronymic', tchr_level = '$level', password = '$password', tchr_email = '$email' WHERE tchr_id = '$id'";
			mysqli_query($connect,$query) or die(mysqli_error($connect));
		} else {
			$query = "UPDATE teachers SET tchr_first_name = '$first_name', tchr_last_name = '$last_name', tchr_patronymic = '$patronymic',
              tchr_level = '$level', tchr_email = '$email' WHERE tchr_id = '$id'";
			mysqli_query($connect,$query);
		}
	}

?>