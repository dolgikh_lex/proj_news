<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/project/doc_header.php');
	if ( $_SESSION['user']['level'] < 3 )
		header("Location: ../profile");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Администраторы</title>
	<link rel="stylesheet" href="/project/css/main.css">
	<link rel="stylesheet" href="/project/admin/admin.css">
	<link rel="stylesheet" href="/project/admin/admin-list/admin-list.css">
	<script src="/project/js/jquery-1.12.3.min.js"></script>
</head>
<body>	
	<?php
		if ($_SESSION['user']){
			$last_name = $_SESSION['user']['last_name'];
			$first_name = substr($_SESSION['user']['first_name'], 0, 2);
			$patronymic = substr($_SESSION['user']['patronymic'], 0, 2);
		}
	?>
	<div class="wrapper">
		<div class="mainContent">
			<div class="header">
				<div class="toolbar">
					<div class="contentWidth">
						<div class="loginName">
							<span class='userName'><?= "$last_name $first_name. $patronymic." ?></span>
							<ul class='navMenu'>
								<li><a href='/project/admin/'>Добавить новость</a></li>
								<li><a href='/project/admin/news'>Список новостей</a></li>
								<li><a href='/project/admin/groups'>Управление группами</a></li>
								<li class='active nav-admins'><a href='/project/admin/admin-list/'>Администраторы</a></li>
							</ul>
						</div>
						<div class="log-in-out">
							<span class='logOut'><a href='/project/?out'> Выйти </a></span>
						</div>
					</div>				
				</div>
			</div>
			<div class="content">
				<div class="contentWidth">
					<div class="breadCrumbs">
						<a href="/project">Главная</a> / Список администраторов
					</div>
					<div class="profile">
						<form class="profile__form" enctype="multipart/form-data" method="POST" action="admins.php">
							<?php
							$tchr_id = $_SESSION['user']['id'];
							$query = "SELECT * FROM teachers WHERE tchr_id = '$tchr_id'";
							$result = mysqli_query($connect,$query);
							$row = mysqli_fetch_assoc($result);
							?>
							<div class="prof-photo">
								<img src=<?php echo $row['tchr_ava_src'] ? $row['tchr_ava_src'] : '/project/img/def-ava.gif' ?>>
								<input type="file" id="ava" name="ava">
								<label for="ava">
									<span>изменить</span>
								</label>
							</div>
							<div class="prof-data">
								<input class="prof-data__login" type="text" value="<?= $row['tchr_last_name'].' '.$row['tchr_first_name'].' '.$row['tchr_patronymic'] ?>" name="login" required><span class="label">Логин</span>
								<br>
								<input type="text" value="<?php echo $row['tchr_email'] ? $row['tchr_email'] : '' ?>" name="email"><span class="label">Email</span>
								<br>
								<input type="submit" class="prof-edit-but" value="Сохранить изменения">
							</div>
						</form>
					</div>
					<div class="new-admin">
						<form action="" method="POST">
							<fieldset>
								<legend>Добавить администратора</legend>
								<input class="new-admin__login" type="text" placeholder="Логин" name="login" required>
								<input class="new-admin__password" type="text" placeholder="Пароль" name="password" required>
								<input class="new-admin__email" type="text" placeholder="Email" name="email">
								<div class="admin-privilege">
									<label>
										<input class="admin-privilege__checkbox groups_control" type="checkbox" name="level[]" value="2">
										<img class="admin-privilege__icon" src="/project/img/mng_gr2.png" alt="">
										<span class="admin-privilege__name">Управление группами</span>
									</label>
									<label>
										<input class="admin-privilege__checkbox admins_control" type="checkbox" name="level[]" value="3">
										<img class="admin-privilege__icon" src="/project/img/cr_user.png" alt="">
										<span class="admin-privilege__name">Управление администраторами</span>
									</label>
								</div>
								<input type="submit" value="Добавить">
							</fieldset>
						</form>
					</div>
					<table class="admin-list">
						<tbody>
							<tr>
								<th></th>
								<th data-order="ask">Логин</th>
								<th data-order="ask">Email</th>
								<th>Уровень доступа</th>
							</tr>
							<?php
								$query = "SELECT * FROM teachers";
								$result = mysqli_query($connect, $query);
								while ( $row = mysqli_fetch_assoc($result) ){
									if ($row['tchr_ava_src'])
										$src = $row['tchr_ava_src'];
									else
										$src = '../../img/def-ava.gif'; ?>
									<tr>
										<td class='admin-ava'><img src=<?=$src?> alt=''></td>
										<td class='admin-login'><?=$row['tchr_last_name'].' '.$row['tchr_first_name'].' '.$row['tchr_patronymic']?></td>
										<td class='admin-email'><?=$row['tchr_email']?></td>
										<td class='admin-level'>
											<?php
												if ($row['tchr_level'] > 1){ ?>
													<img src="/project/img/mng_gr2.png" alt="Управление группами" title="Управление группами" data-level="2">
												<?php }
												if ($row['tchr_level'] > 2){ ?>
													<img src="/project/img/cr_user.png" alt="Управление администраторами" title="Управление администраторами" data-level="3">
												<?php } ?>
										</td>
										<td><img class='admin-edit' data-id='<?=$row['tchr_id']?>' src='../../img/edit.png' title='Редактировать'><img class='admin-delete' data-id='<?=$row['tchr_id']?> ' src='../../img/close.png' title='Удалить'></td>
									</tr>
								<?php } ?>
						</tbody>
					</table>
				</div>
				
			</div>
		</div>
		<?php require_once('../footer.php');?>
	</div>
	<script src="../../js/admin-list.min.js"></script>
</body>
</html>