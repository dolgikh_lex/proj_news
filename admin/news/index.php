<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/project/doc_header.php')
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="/project/css/main.css">
	<link rel="stylesheet" href="/project/admin/admin.css">
	<link rel="stylesheet" href="news.css">
	<script src="/project/js/jquery-1.12.3.min.js"></script>
</head>
<body>	
	<?php
		if ($_SESSION['user']){
			$last_name = $_SESSION['user']['last_name'];
			$first_name = substr($_SESSION['user']['first_name'], 0, 2);
			$patronymic = substr($_SESSION['user']['patronymic'], 0, 2);
		}
	?>
	<div class="wrapper">
		<div class="mainContent">
			<div class="header">
				<div class="toolbar">
					<div class="contentWidth">
						<div class="loginName">
							<span class='userName'><?= "$last_name $first_name. $patronymic." ?></span>
							<ul class='navMenu'>
								<li><a href='/project/admin/'>Добавить новость</a></li>
								<li class='active'><a href='/project/admin/news/'>Список новостей</a></li>
								<li><a href='/project/admin/groups'>Управление группами</a></li>
								<?php
									$last_item_menu_text = 'Профиль';
									$last_item_menu_url = 'profile';
									if ( $_SESSION['user']['level'] == 3 ){
										$last_item_menu_text = 'Администраторы';
										$last_item_menu_url = 'admin-list';
									} 
								?>
								<li class='nav-admins'><?= "<a href='/project/admin/$last_item_menu_url'>$last_item_menu_text</a>" ?></li>
							</ul>
						</div>
						<div class="log-in-out">
							<span class='logOut'><a href='/project/?out'> Выйти </a></span>
						</div>
					</div>				
				</div>
			</div>
			<div class="content">
				<div class="contentWidth">
					<div class="breadCrumbs">
						<a href="/project/">Главная</a> / Список новостей
					</div>
						<div class="news">
							<?php
									$newsCountPage = 10;
									if ( !isset($_GET['page']) ){
										$page = 0;
										$limit = '0,'.$newsCountPage;
									} else {
										$page = $_GET['page'];
										$limit = $newsCountPage * $page - $newsCountPage.','.$newsCountPage;
										
									}		
									// общее количество новостей для выбранного человека					
									$query1 = "SELECT * FROM news n LEFT JOIN teachers t ON n.author = t.tchr_id";
									$result = mysqli_query($connect, $query1);
									$newsCount = mysqli_num_rows($result);
									$pagesCount = (ceil ($newsCount / $newsCountPage) );

									// новости для выбранной страницы
									$query = "SELECT * FROM news n LEFT JOIN teachers t ON n.author = t.tchr_id ORDER BY n.new_id DESC LIMIT $limit";
									$result = mysqli_query($connect, $query) or die( mysqli_error($connect) );
									while ( $row = mysqli_fetch_array($result) ){
										
										$ids[] = $row['new_id'];
										$authorName = $row['tchr_last_name'].' '.substr($row['tchr_first_name'], 0, 2).'. '.substr($row['tchr_patronymic'], 0, 2).'.';
										
										echo "<div class='new' id='{$row['new_id']}' title='Редактировать новость'>
												<div class='newContent'>
												<div class='upper'>
													<h3><a class='newHeading' href='#'>{$row['heading']}</a></h3>
													<div class='info'>
														<span class='newDate'>{$row['pub_date']}</span>
														<span class='newAuthor'>$authorName</span>
													</div>
												</div>";
											$subText = substr($row['text'], 0, 300).'...';
											echo "<div class='newText'>$subText</div>
											<div class='clearfix'></div>";
											$query6 = "SELECT * FROM new_receiver WHERE new_id = {$row['new_id']}";
											$result1 = mysqli_query($connect, $query6);
											$qtyAll = mysqli_num_rows($result1); 
											$read = $see = $noSee = 0;
											while( $row1 = mysqli_fetch_assoc($result1) ){
												if ($row1['read_status'] == 0) $noSee+= 1;
												if ($row1['read_status'] == 1) $see+= 1;
												if ($row1['read_status'] == 2) $read+= 1;
											}
											$noSee = round($noSee * 100 / $qtyAll, 1);
											$see = round($see * 100 / $qtyAll, 1);
											$read = round($read * 100 / $qtyAll, 1);
											echo "<div class='statistic'>
													<ul>
													 <li><span class='percent'>$read%</span> отметили</li>
													 <li><span class='percent'>$see%</span> увидели</li>
													 <li><span class='percent'>$noSee%</span> не смотрели</li>
													</ul>
												</div>";
											// закрытие newContent и new
											echo "</div></div>";
									
									}
									echo "</div>";
									if ($pagesCount > 1){
										$currentPage = $_GET['page'];
										if (!$currentPage) $currentPage = 1;
										$nextPage = $currentPage+1;
										$prevPage = $currentPage-1;
										echo "<div class='paginator'>
										<ul>";
										if ( isset($_GET['page']) && $_GET['page'] != 1) echo "<li class='nextPage prevPage'><a href='./?page=$prevPage'></a></li>";

										// если количество страниц менее 6, фомируем список с пропусками
										if ($pagesCount > 6){
											if ($currentPage > 2){
												echo "<li><a href='./?page=1'>1</a></li>";
												if ($currentPage > 3){
													echo "<li>...</li>";
												}

											}
											if ($currentPage > 1 && $currentPage < 5){
												echo "<li><a href='./?page=$prevPage'>$prevPage</a></li>";
											}
											if ($pagesCount - $currentPage > 3){
												echo "<li class='activePage'><a href='./?page=$currentPage'>$currentPage</a></li>
														<li><a href='./?page=$nextPage'>$nextPage</a></li><li>...</li>";
											} else {
												for ($i=$pagesCount-3; $i < $pagesCount; $i++){
													$next = $i+1;
													echo "<li";
													if ($currentPage == $i) echo " class='activePage'";
													echo "><a href='./?page=$i'>$i</a></li>
															";
												}
											}
											echo "<li";
											if ($currentPage == $pagesCount) echo " class='activePage'";
											echo "><a href='./?page=$pagesCount'>$pagesCount</a></li>";
										} else {
											$i = 1;
											while ( $i != $pagesCount+1){
												echo "<li";
												if ($i == $currentPage) echo " class='activePage'";
												echo "><a href='./?page=$i'>$i</a></li>";
												$i++;
											}
										}
										if ($_GET['page'] != $pagesCount) echo "<li class='nextPage'><a href='./?page=$nextPage'></a></li>";
										echo "</ul></div>";
									}
							?>
					</div>
				</div>
			</div>
			<?php require_once('../footer.php');?>
	</div>
	<script>
	function editPage(){
		id = this.id;
		window.location.href = './detail?id=' + id;
	}
		var news = document.querySelectorAll('.new');
		for (var i = 0; i < news.length; i++) {
		  news[i].addEventListener("click", editPage);
		}
	</script>
</body>
</html>