<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/project/doc_header.php')
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="/project/css/main.css">
	<link rel="stylesheet" href="/project/admin/admin.css">
 	<link rel="stylesheet" href="newInfo.css">
	<script src="/project/js/jquery-1.12.3.min.js"></script>
</head>
<body>	
	<?php
		if ($_SESSION['user']){
			$last_name = $_SESSION['user']['last_name'];
			$first_name = substr($_SESSION['user']['first_name'], 0, 2);
			$patronymic = substr($_SESSION['user']['patronymic'], 0, 2);
		}
	?>
	<div class="wrapper">
		<div class="mainContent">
				<div class="header">
					<div class="toolbar">
						<div class="contentWidth">
							<div class="loginName">
								<span class='userName'><?= "$last_name $first_name. $patronymic." ?></span>
								<ul class='navMenu'>
									<li><a href='/project/admin/'>Добавить новость</a></li>
									<li class='active'><a href='/project/admin/news/'>Список новостей</a></li>
									<li><a href='/project/admin/groups/'>Управление группами</a></li>
									<?php
										$last_item_menu_text = 'Профиль';
										$last_item_menu_url = 'profile';
										if ( $_SESSION['user']['level'] == 3 ){
											$last_item_menu_text = 'Администраторы';
											$last_item_menu_url = 'admin-list';
										} 
									?>
									<li class='nav-admins'><?= "<a href='/project/admin/$last_item_menu_url'>$last_item_menu_text</a>" ?></li>
								</ul>
							</div>
							<div class="log-in-out">
								<span class='logOut'><a href='/project/?out'> Выйти </a></span>
							</div>
						</div>				
					</div>
				</div>
				<div class="content">
					<div class="contentWidth">
						<div class="breadCrumbs">
							<a href="/project/">Главная</a> / <a href="/project/admin/news/">Список новостей</a> / Информация о новости
						</div>
						
						<?php
							$new_id = $_GET['id'];
							$query1 = "SELECT * FROM news n LEFT JOIN teachers t ON n.author=t.tchr_id LEFT JOIN files f ON n.new_id=f.new_id WHERE n.new_id=$new_id";
							$result1 = mysqli_query($connect, $query1);
							$row = mysqli_fetch_assoc($result1);
								$authorName = $row['tchr_last_name'].' '.substr($row['tchr_first_name'], 0, 2).'. '.substr($row['tchr_patronymic'], 0, 2).'.';
								
								echo "<div class='new'>
										<div class='newContent'>
										<div class='upper'>
											<h3>{$row['heading']}</h3>
											<div class='detail'>
												<span class='newDate'>{$row['pub_date']}</span>
												<span class='newAuthor'>$authorName</span>
											</div>
										</div>";

								if ($row['img']){									
									echo "<div class='newImg' style=\"background-image: url('../../../{$row['img']}');\"></div>";
								}
									echo "<div class='newText'>{$row['text']}</div>
									<div class='clearfix'></div>";

									$query5 = "SELECT * FROM files WHERE new_id = {$row['new_id']}";
									$result2 = mysqli_query($connect, $query5);
									if ( mysqli_num_rows($result2) > 0 ){
										echo "<div class='files'> <ul>";
										
										while( $row2 = mysqli_fetch_assoc($result2) ){
											echo "<li><a href='{$row2['file_src']}'>{$row2['file_name']}</a></li>";
										}
										echo "</ul></div>";
									}
							echo "<div class='deleteNew'><a href='deleteNew.php?id=$new_id'>Удалить новсоть</a></div>";
							// закрыть теги(классы) newContent и new
							echo "</div></div>";
							$query = "SELECT * FROM new_receiver ns LEFT JOIN students s ON ns.std_id=s.std_id LEFT JOIN std_group sg ON s.std_id = sg.std_id LEFT JOIN groups g ON sg.group_id = g.group_id  WHERE ns.new_id=$new_id GROUP BY s.std_id ORDER BY read_status DESC";
							$result = mysqli_query($connect, $query);

						?>
						<h3 class="tableTitle">Список студентов новости</h3>
						<table class="students">
							<tbody>
								<tr>
									<th data-order="ask">Фамилия Имя</th>
									<th data-order="ask">Группа</th>
									<th data-order="ask">Статус</th>
								</tr>
								<?php
									while( $row=mysqli_fetch_assoc($result) ){
										switch ( $row['read_status'] ) {
											case 0:
												$read_status = 'не смотрел';
												$read_class = 'statusNo';
												break;
											case 1:
												$read_status = 'увидел';
												$read_class = 'statusSaw';
												break;
											case 2:
												$read_status = 'отметил';
												$read_class = 'statusChecked';
												break;	
										}
										echo "<tr class='$read_class'>
												<td>{$row['last_name']} {$row['first_name']}</td>
												<td>{$row['name']}</td>
												<td>$read_status</td>
											</tr>";
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		<?php require_once('../../footer.php');?>
	</div>
	<script>
		document.querySelector('.deleteNew a').onclick = function(){
			if ( !confirm("Вы уверены, что хотите удалить новость?") ) return false;			
		}
	</script>
	<script src="../../../js/sort.min.js"></script>
</body>
</html>