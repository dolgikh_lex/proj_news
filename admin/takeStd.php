<?php
    if ( isset($_POST['arGroupIds']) ){
        $arGroupIds = $_POST['arGroupIds'];
        $arStdsByGroupId = array();
        $stdIds = array();
        $arRepeatedStdIds = array();
        include('../dbconnect.php');
        $arGroupIds = join(',', $arGroupIds);
        $query = "SELECT * FROM std_group sg LEFT JOIN students s ON s.std_id = sg.std_id LEFT JOIN groups g ON sg.group_id = g.group_id 
                    WHERE sg.group_id IN ($arGroupIds) ORDER BY s.last_name";
        $result = mysqli_query($connect, $query);
        if (isset($_POST['activity'])){ // если запрашиваем активность со страницы групп
            while ( $row = mysqli_fetch_array($result) ){
                $query1 = "SELECT * FROM new_receiver WHERE std_id='{$row['std_id']}'";
                $result1 = mysqli_query($connect, $query1);
                $allNews = mysqli_num_rows($result1);
                $checkedNews = 0;
                while ( $row1 = mysqli_fetch_assoc($result1) ){
                    if ($row1['read_status'] == 2) $checkedNews+=1;
                }
                if ($allNews == 0){
                    $activatyPerc = 0;
                } else{
                    $activatyPerc = round($checkedNews * 100 / $allNews, 1);
                }
                $arStdsByGroupId[$row['group_id']][] = array('name'=>$row['last_name'].' '.$row['first_name'], 'group'=>$row['name'], 'sId'=>$row['std_id'], 'activity'=>$activatyPerc);
            }
        } else {
            while ( $row = mysqli_fetch_array($result) ){
                $arStdsByGroupId[$row['group_id']][] = array('name'=>$row['last_name'].' '.$row['first_name'], 'group'=>$row['name'], 'sId'=>$row['std_id']);
                if ( in_array($row['std_id'], $stdIds) ){
                    $arRepeatedStdIds[$row['std_id']] = 0;
                } else {
                    $stdIds[] = $row['std_id'];
                }
            }
        }
        $arStudents = array('stdList'=>$arStdsByGroupId, 'repeatedStds'=>$arRepeatedStdIds);
        echo json_encode($arStudents);
    } else {
        die();
    }

//	if ($group == 'all'){
//		$query = "SELECT * FROM students s LEFT JOIN std_group sg ON s.std_id = sg.std_id LEFT JOIN groups g ON sg.group_id = g.group_id ORDER BY s.last_name";
//		$result = mysqli_query($connect, $query);
//
//		while ( $row = mysqli_fetch_array($result) ){
//			$students[] = array('name'=>$row['last_name'].' '.$row['first_name'], 'group'=>$row['name']);
//		}
//	} else{
//		$query = "SELECT * FROM students s LEFT JOIN std_group sg ON s.std_id = sg.std_id LEFT JOIN groups g ON sg.group_id = g.group_id WHERE g.name LIKE '$group' ORDER BY s.last_name";
//		$result = mysqli_query($connect, $query);
//
//		while ( $row = mysqli_fetch_array($result) ){
//			$students[] = $row['last_name'].' '.$row['first_name'];
//		}
//	}
?>